#ifndef module_hpp
#define module_hpp

#include <thread>

#include "port.hpp"

class Module {
private:
    std::thread *t;
public:
    // virtual function to be implemented by subclass module
    virtual void compute()=0;
    
    // interface to run modules
    void run();
    void start();
    void join();

};

#endif /* module_hpp */
