#include "workflow.hpp"

#include <iostream>
#include <mutex>


Workflow::Workflow(){}

Workflow::Workflow(std::string name){
    wname = name;
}

Workflow::~Workflow(){
    //TODO: delete modules at the end of the workflow
}

void Workflow::addModule(Module& module){
    modules.push_back(&module);
}

void Workflow::link(OPort& writer, IPort& reader){
        reader.init(writer.init());
}

void Workflow::link_outputs(OPort& upper, OPort& inner){
        inner.set_q(upper.get_q());
}

void Workflow::link_inputs(IPort& upper, IPort& inner){
        void * q = upper.get_q();
        inner.init(q);
}

void Workflow::start(){
    
    beg = std::chrono::system_clock::now();
    
    // run threads
    for (auto m : modules){
        m->start();
    }

    // join all threads
    for (auto m : modules){
        m->join();
    }
    
    end = std::chrono::system_clock::now();
    
    if(wname.size() > 0){
        lock.lock();
        std::chrono::duration<double> elapsed_seconds = end-beg;
        std::cout << wname << ": elapsed time " << elapsed_seconds.count() << " s" << std::endl;
        lock.unlock();
    }

}
