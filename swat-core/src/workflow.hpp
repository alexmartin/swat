#ifndef workflow_hpp
#define workflow_hpp

#include "module.hpp"

#include <list>
#include <chrono>
#include <string>

// mutex to lock stdout
static std::mutex lock;

class Workflow {

private:
    // timer for execution time
    std::chrono::time_point<std::chrono::system_clock> beg, end;
        
    // module list
    std::list<Module*> modules;
    
    // workflow name
    std::string wname;
    

public:

    Workflow();

    Workflow(std::string name);
    
    ~Workflow();
    
    // add a module to the workflow
    void addModule(Module& module);
    
    // links two modules ports input/output
    void link(OPort& writer, IPort& reader);
    
    // used for subworkflow, link 2 output ports
    void link_outputs(OPort& upper, OPort& inner);
    
    // used for subworkflow, link 2 input ports
    void link_inputs(IPort& upper, IPort& inner);
    
    // run the workflow, waits until all modules completion.
    void start();

};
#endif /* workflow_hpp */
