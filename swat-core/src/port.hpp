#ifndef port_hpp
#define port_hpp

//TODO: make it dynamic
#define BUFF_SIZE 1000
#define MAX_READERS 10

#include "demuxq.hpp"

// type to specify data mode transfert
typedef enum mode {Value, Stream} data_mode_t;

// exception raised when reading a ended stream
class end_of_data: public std::exception{
public:
    virtual const char* what() const throw(){
        return "End of data";
    }
};

// IPort and OPort: abstract classes for ports 
class IPort {
public:
    virtual void init(void * q_ptr)=0;
    virtual void set_q(void * q_ptr)=0;
    virtual void* get_q()=0;
};

class OPort {
public:
    virtual void* init()=0;
    virtual void set_q(void * q_ptr)=0;
    virtual void* get_q()=0;
};

// Generic definition of InputPort
template <typename T, data_mode_t M> class InputPort: virtual public IPort{};


// partial specialization for stream
template <typename T>
class InputPort<T, Stream>: virtual public IPort {
    
public:
    
    Demuxq<T> *queue = nullptr;
    int id;

    //TODO: notify upstrem that reader is done with this port
    // this is optimisation to avoid unneccesary computation
    //void close();
    
    T recv(){
        try{
            return queue->pop(id);
        }catch (closed_exception){
            throw end_of_data();
        }
    }
    
    void init(void* q_ptr){
        queue = (Demuxq<T>*) q_ptr;
        id = queue->get_new_reader_id();
    }
    
    bool is_used(){
        return (queue != nullptr and queue->get_num_readers() > 0 );
    }

    void set_q(void * q_ptr){
        queue = (Demuxq<T>*) q_ptr;
    }
    
    void* get_q(){
        return queue;
    }
    
};


// partial specialization for value
template <typename T>
class InputPort<T, Value>: virtual public IPort {
    
public:
    
    Demuxq<T> *queue = nullptr;
    int id;
    
    T get(){
        try{
            return queue->pop(id);
        }catch (closed_exception){
            throw end_of_data();
        }
    }
    
    void init(void* q_ptr){
        queue = (Demuxq<T>*) q_ptr;
        id = queue->get_new_reader_id();
    }
    
    bool is_used(){
        return (queue != nullptr and queue->get_num_readers() > 0 );
    }

    void set_q(void * q_ptr){
        queue = (Demuxq<T>*) q_ptr;
    }
    
    void* get_q(){
        return queue;
    }
    
};

// Generic definition of OutputPort
template <typename T, data_mode_t M> class OutputPort: virtual public OPort {};


template <typename T>
class OutputPort<T, Stream>: virtual public OPort {
    
public:
    
    Demuxq<T> *queue = nullptr;
    
    void send(T element){
        // only push value if the port is configured
        if(queue != nullptr) {
            queue->push(element);
        }
    }
    
    // close the queue
    void close(){
        if(queue != nullptr){
            queue->close();
        }
    }
    
    // used for optimization return true if someone will use the port
    // example : if(port.used()){port.send(do_heavy_computation);}
    bool is_used(){
        return (queue != nullptr and queue->get_num_readers() > 0 );
    }
    
    void* init(){
        void * q_ptr;
        if(queue == nullptr){
            q_ptr = new Demuxq<T>(BUFF_SIZE, MAX_READERS);
            queue = (Demuxq<T>*) q_ptr;
        }else{
            q_ptr = queue;
        }
        return q_ptr;
    }
    
    void set_q(void * q_ptr){
        queue = (Demuxq<T>*) q_ptr;
    }
    
    void* get_q(){
        return queue;
    }

};


template <typename T>
class OutputPort<T, Value>: virtual public OPort {
private:
     Demuxq<T> *queue = nullptr;
public:
    
    void set(T element){
        // set a final value for the port
        if(queue != nullptr) {
            queue->push(element);
            // automatically close the queue
            queue->close();
        }
    }
    
    // used for optimization return true if someone will use the port
    // example : if(port.used()){port.send(do_heavy_computation);}
    bool is_used(){
        return (queue != nullptr and queue->get_num_readers() > 0 );
    }
    
    void* init(){
        void * q_ptr;
        if(queue == nullptr){
            q_ptr = new Demuxq<T>(1, MAX_READERS); //TODO: use a lighter data structure
            queue = (Demuxq<T>*) q_ptr;
        }else{
            q_ptr = queue;
        }
        return q_ptr;
    }
    
    void set_q(void * q_ptr){
        queue = (Demuxq<T>*) q_ptr;
    }
    
    void* get_q(){
        return queue;
    }
};

#endif /* port_hpp */
