#include "module.hpp"

#include <functional>

void Module::run(){
    
    // run user code
    //TODO: try catch to stop compute if no data is consumed after
    //TODO: module may want to run until the end event no data is produced ?
    compute();
    
    
    //TODO: finish : close all ports
}

void Module::start(){
    // TODO: implement cache mechanism
    t = new std::thread (std::bind(&Module::run, this));
}

void Module::join(){
    t->join();
}
