#ifndef demuxq_hpp
#define demuxq_hpp

#include <cstdlib>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <exception>

/*
    A Demuxq is a queue with one writer and several readers.
    All readers reads all data in the queue.
    Inmplemented on a rig buffer structure.
    Demuxq stand for demultiplexed queue.
*/

class closed_exception: public std::exception{
public:
    virtual const char* what() const throw(){
        return "Closed";
    }
};

template <class T>
class Demuxq{
private:
    // data buffer
    T *buff;

    // begin and end of buffer
    int beg;  // buffer empty when beg == end
    int end;  // buffer full when end - beg == max_size -1
    
    // array that stores the position of readers
    int *reader_pos;
    
    // queue sizes
    int max_size;
    int max_readers;
    // number of reader of the demuxq
    int num_readers = 0;
    
    uint64_t writes = 0;
    uint64_t reads = 0;

    
    // mutex for read and writes
    std::mutex lock;
    
    // condition for readers
    std::condition_variable readers_cond;
    std::condition_variable writer_cond;
    
    // boolean to tell if readers are sleeping
    bool readers_wait = false;
    // true when buffer full, used to notify only when needed
    bool writer_waits = false;
    
    // is pipe closed ( = no more data written )
    bool closed = false;
    
    inline int get_delta(int beg, int end){
        int delta = end - beg;
        // adjust for negative positions
        if(delta < 0){
            delta += max_size;
        }
        return delta;
    }
    
    // TODO: test perfs over get_delta
    inline int get_delta_modulo(int beg, int end){
        return (end - beg) % max_size;
    }
    
    inline void incr(int *ptr, int i){
        *ptr += i;
        if(*ptr >= max_size){
            *ptr %= max_size;
        }
    }
    
public:
    
    Demuxq(int buff_size, int readers){
        // allocate buffer
        max_size = buff_size + 1; // +1 is for the extra unused element of the buffer.
        buff = new T[max_size];
        beg = 0;
        end = 0;
        // allocate position arrays
        reader_pos = new int[readers] {};
        max_readers = readers;
    }
    
    ~Demuxq(){
        free(buff);
        free(reader_pos);
    }
    
    // add a reader to the pipe and return a reader id
    // if no more reader possible, return -1
    //TODO: throw exeption ?
    int get_new_reader_id(){
        if(num_readers < max_readers){
            // id of the reader is num of readers
            int reader_id = num_readers++;
            // init reader position
            return reader_id;
        }else{
            return -1;
        }
    }
    
    int get_num_readers(){
        return num_readers;
    }
    
    //close the queue, no more data will be writen again
    void close(){
        std::unique_lock<std::mutex> auto_lock(lock);
        closed = true;
        // notify pending readers
        readers_cond.notify_all();
    }
    
    // push an element in the queue,
    // notify readers if threads are waiting on data ( = queue was previously empty)
    void push(T element){
        std::unique_lock<std::mutex> auto_lock(lock);
        
        int delta = get_delta(beg, end);
        
        // check if buffer is full and try to shrink it if data read by all readers
        while (delta == max_size - 1){
            // reposition the begin pointer
            int min_delta = max_size;
            for(int i=0; i<num_readers; i++){
                int ri = reader_pos[i];
                // compute local delta for reader i
                int delta = get_delta(beg, ri);
                if(delta < min_delta){
                    min_delta = delta;
                }
            }
            // if still full, waits (ie beg == ri -> min_delta == 0)
            if(min_delta == 0){
                writer_waits = true;
                writer_cond.wait(auto_lock);
            }else{
                // set new beg
                incr(&beg, min_delta);
                break;
            }
            
        }
        
        // copy element in the buffer
        buff[end] = element;
        
        // inc end position
        incr(&end, 1);
        
        // wake up any reader if necessary
        if(readers_wait){
            // notyfy all
            readers_wait = false;
            readers_cond.notify_all();
        }
        
        writes++;
        
    }
    
    // pop and return an element from the queue
    // notify writer if waiting on write ( = thre reader is the last reader)
    T pop(int id) throw (closed_exception){
        std::unique_lock<std::mutex> auto_lock(lock);
        
        int pos = reader_pos[id];
        int delta = get_delta(pos, end);
        
        // no data to read
        if(delta == 0){
            if(closed){
                throw closed_exception();
            }else{
                readers_wait = true;
                // waits data to be written
                readers_cond.wait(auto_lock);
                
                // update delta
                delta = get_delta(pos, end);
                //check cause of notify (data or closed)
                if(closed and delta == 0){
                    throw closed_exception();
                }
            }
        }
        
        T e = buff[pos];
        incr(&reader_pos[id], 1);
        
        // if reading get the buffer free for new write
        if(writer_waits){
            // notify writer
            writer_waits = false;
            writer_cond.notify_one();
        }
        reads++;
        return e;
    }
    
};


#endif /* demuxq_hpp */
