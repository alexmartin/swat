#ifndef streamtovalues_h
#define streamtovalues_h

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>


template <class T>
class StreamToValues: virtual public Module {
private:
    int size;
public:
    
    InputPort<T, Stream> datastream;
    OutputPort<T, Value> *datavalue;
    
    StreamToValues(int size){
        this->size = size;
        datavalue = new OutputPort<T, Value> [size];
    }
    
    void compute(){
        
        T e;
        
        //TODO: error when size is greater than the stream, plus what to do with unused values
        for(int i=0; i<size; i++){
            // get next event
            try {
                e = datastream.recv();
            } catch (end_of_data) {
                break;
            }
            
            datavalue[i].set(e);

        }
        
    }
    
};


#endif /* streamtovalues_h */
