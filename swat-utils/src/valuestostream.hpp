#ifndef valuestostream_hpp
#define valuestostream_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>


template <class T>
class ValuesToStream: virtual public Module {
private:
    int size;
    
public:
    
    InputPort<T, Value> *data;
    OutputPort<T, Stream> datastream;
    
    ValuesToStream(int size){
        this->size = size;
        data = new InputPort<T, Value> [size];
    }
    
    void compute(){
        for (int i=0; i<size; i++) {
            T d = data[i].get();
            datastream.send(d);
        }
        datastream.close();
    }

};

#endif /* valuestostream_hpp */
