#ifndef stat_hpp
#define stat_hpp

#include <swat/core/type.hpp>
#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "constant.hpp"

#include <list>
#include <cmath>
#include <iostream>
#include <algorithm>

//type stat definition
// T must implement < operator
template <class T>
class Stat: virtual public GenericType{
    
private:
    std::list<T> l;
    std::string name;
    
public:
    Stat(){};
    
    Stat(std::string name){
        this->name = name;
    };

    friend std::ostream& operator<<(std::ostream& stream,  Stat st){
        stream << "elems";
        for(auto e: st.l){
            stream << "," << e;
        }
        
        stream << std::endl;
        stream << "mean,std,min,max,sum" << std::endl;
        stream << st.mean() << "," << st.std() << "," << st.min() << "," << st.max() << "," << st.sum() << std::endl;
        return stream;
        
    };
    
    // add a value
    void add(T v){
        l.push_front(v);
    };
    
    //minimum value
    const T min(){
        return *std::min_element(l.begin(), l.end());
    };
    
    //maximum value
    T max(){
        return *std::max_element(l.begin(), l.end());
    };
    
    //standard deviation in percentage of the mean
    float std(){
        float m = mean();
        T sum=0.0;
        long n = size();
        for(auto e: l){
            sum += std::pow(e-m,2);
        }
        return std::sqrt(sum/(float)n)/m*100; // percentage
    };
    
    float mean(){
        return sum()/(float)size();
    }
    
    T sum(){
        T sum = 0; // valide for int long float...
        for(auto e: l){
            sum += e;
        }
        return sum;
    };
    
    //percentage ? 
    
    //number of elements
    long size(){
        return l.size();
    };
    
};


//generic module to compute stats
template <class T>
class ComputeStat: virtual public Module {
public:
    
    InputPort<T, Stream> data_in;
    OutputPort<Stat<T>, Value> stat;
    
    void compute(){
        Stat<T> s;
        while (true) {
            try {
                T v = data_in.recv();
                s.add(v);
            } catch (end_of_data){
                break;
            }
        }
        stat.set(s);
    }
};




// specialisation of the constant module for stat type to add extra outputs
template <class T>
class ConstantModule<Stat<T>> : public Constant<Stat<T>> {
public:
    //additional output ports
    OutputPort<T, Value> min;
    OutputPort<T, Value> max;
    OutputPort<T, Value> std;
    
    // inherite constructors
    using Constant<Stat<T>>::Constant;
    
    void compute(){
        // inherite compute form parent class
        Constant<Stat<T>>::compute();
        
        if(min.is_used()){
            min.set(Constant<Stat<T>>::v.max());
        }
        
        if(max.is_used()){
            max.set(Constant<Stat<T>>::v.min());
        }
        
        if(std.is_used()){
            std.set(Constant<Stat<T>>::v.std());
        }
        
    }
    
};



// Specialisation for counter class

#include "counter.hpp"

template <class K, class T>
class Stat<Counter<K, T>>: virtual public GenericType{
    
private:
    std::map<K,Stat<T>> m;
    
public:
    Stat(){};
    
    friend std::ostream& operator<<(std::ostream& stream,  Stat st){
        stream << "mean: " << std::endl <<st.mean();
        stream << "std: " << std::endl << st.std();
        stream << "min: " << std::endl <<st.min();
        stream << "max: " << std::endl <<st.max();
        stream << "sum: " << std::endl <<st.sum();
        return stream;
    };
    
    // add a value
    void add(Counter<K, T> v){
        for (auto pair: v.get_map()) {
            m[pair.first].add(pair.second);
        }
    };
    
    //minimum value
    // return Counter with all minimums values
    Counter<K,T> min(){
        
        Counter<K,T> min;
        for (auto pair: m) {
            min.insert(pair.first, m[pair.first].min());
        }

        return min;
    };
    
    //maximum value
    // return Counter with all maximum values
    Counter<K,T> max(){
        
        Counter<K,T> max;
        for (auto pair: m) {
            max.insert(pair.first, m[pair.first].max());
        }
        
        return max;

    };
    
    //standard deviation
    // return Counter with all stdev values
    Counter<K,float> std(){

        Counter<K,float> std;
        for (auto pair: m) {
            std.insert(pair.first, m[pair.first].std());
        }
        
        return std;

    };
    
    // return Counter with all means values
    Counter<K,float> mean(){
        
        Counter<K,float> mean;
        for (auto pair: m) {
            mean.insert(pair.first, m[pair.first].mean());
        }
        
        return mean;

    }
    
    Counter<K,T> sum(){
        
        Counter<K,T> sum;
        for (auto pair: m) {
            sum.insert(pair.first, m[pair.first].sum());
        }
        
        return sum;
    };
    
    //percentage ?
    
    //number of elements
    long size(){
        return m.size();
    };
    
};

#endif /* stat_hpp */
