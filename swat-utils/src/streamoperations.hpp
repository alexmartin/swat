#ifndef streamoperations_hpp
#define streamoperations_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>

template <class T>
class SkipElements: virtual public Module {
public:
    InputPort<T, Stream> datastream_in;
    InputPort<int, Value> n;

    OutputPort<T, Stream> datastream_out;
    
    void compute(){
        
        T e;
        
        int nb_skip = n.get();
        
        //skip first n elements
        //TODO: check if nb_skip < stream size
        for (int i=0; i<nb_skip; i++) {
            datastream_in.recv();
        }
        
        // forward other elements
        while(true){
            try {
                e = datastream_in.recv();
            } catch (end_of_data) {
                break;
            }
            
            datastream_out.send(e);
        }

        
        datastream_out.close();
    }
    
};

#endif /* streamoperations_hpp */
