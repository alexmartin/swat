#ifndef constant_hpp
#define constant_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>

// Constant definition to insert static value in the workflow or
// get special values if template is specialized for custom types (cf stat.hpp)

template <class T>
class Constant: virtual public Module {
protected:
    T v;
    bool value_is_set;
public:
    InputPort<T, Value> constant_value_in;
    OutputPort<T, Value> constant_value_out;
    
    Constant(){
        value_is_set = false;
    }
    
    Constant(T value){
        value_is_set = true;
        v = value;
    }
    
    void compute(){
        if (not value_is_set){
            v = constant_value_in.get();
        }
        constant_value_out.set(v);
    }
    
};

// template to be specialized for used-defined type.
template <class T>
class ConstantModule: public Constant<T> {
public:
    using Constant<T>::Constant;
};
#endif /* constant_hpp */
