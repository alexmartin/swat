#include "stringoperation.hpp"

#include <string>

RemoveFirstChar::RemoveFirstChar(int size){
    this->size = size;
}

void RemoveFirstChar::compute(){
    
    std::string s;
    
    while(true){
        try {
            s = input_stream.recv();
        } catch (end_of_data) {
            break;
        }

        output_stream.send(s.substr(size, s.size()));
        
    }
    
    output_stream.close();
    
}


void StringToFloat::compute(){
    
    std::string s;
    float res;
    
    while(true){
        try {
            s = string_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
        if (s.size()>0){
            res = std::stof(s);
        }else{
            res = 0;
        }
        
        float_stream.send(res);
        
    }
    
    float_stream.close();
    
}


void StringToUint64::compute(){
    
    std::string s;
    uint64_t res;
    
    while(true){
        try {
            s = string_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
        if (s.size()>0){
            res = std::stoull(s);
        }else{
            continue;
        }
        
        ull_stream.send(res);
        
    }
    
    ull_stream.close();
    
}


void StringSplitter::compute(){
    
    std::string s = i_string_stream.get();
    const char splt = splitter.get();
    std::string token;
    std::string::size_type n;
    
    while(true){
        
        n = s.find(splt);
        
        if (n != std::string::npos){
            
            token = s.substr(0,n);
            
            o_string_stream.send(token);
            
            s = s.substr(n+1);
            
        } else {
            
            //send last chunk
            o_string_stream.send(s);
            break;
        
        }
        
    }
    
    o_string_stream.close();
    
}
