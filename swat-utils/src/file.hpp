#ifndef file_hpp
#define file_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include <fstream>

template <class T>
class WriteFile: public Module{
public:
    InputPort<std::string, Value> path;
    InputPort<T, Value> content;
    
    void compute(){
        
        std::fstream fs;
        
        std::string spath = path.get();
        
        fs.open(spath, std::fstream::out);

        fs << content.get();
        
        fs.close();
    
    }
    
};

template <class T>
class ReadFile: public Module{
public:
    InputPort<std::string, Value> path;
    OutputPort<T, Stream> content;
    
    void compute(){
        
        std::fstream fs;
        
        fs.open(path.get(), std::fstream::in);
        
        T data;
        while(!fs.eof()){
            std::getline(fs,data);
            content.send(data);
        }
        
        fs.close();
        content.close();
        
    }
    
};

#endif /* file_hpp */
