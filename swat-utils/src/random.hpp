#ifndef random_hpp
#define random_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>

#include <random>

// generate random values
// if num_values = 0, generate infinit stream
template <class T>
class Random: virtual public Module{
public:
    // input ports
    InputPort<int, Value> num_values;
    OutputPort<T, Stream> random;
    
    void compute(){
        int n = num_values.get();
        while(n>0){
            T r;
            random.send(r);
            --n;
        }
        
        //manually close stream output at the moment
        random.close();
        
    };
};


template <>
class Random<int>: virtual public Module{
public:
    // input ports
    InputPort<int, Value> num_values;
    OutputPort<int, Stream> random;
    
    void compute(){
        int n = num_values.get();
        while(n>0){
            int r = rand();
            random.send(r);
            --n;
        }
        
        //manually close stream output at the moment
        random.close();
        
    };
};

#endif /* random_hpp */
