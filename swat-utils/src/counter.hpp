#ifndef counter_hpp
#define counter_hpp

#include <swat/core/type.hpp>
#include <swat/core/module.hpp>

#include "constant.hpp"

#include <map>
#include <string>

#include <iostream>

template <class Key, class T>
class Counter: virtual public GenericType {
private:
    std::map<Key,T> map;
    std::string name = "";
    
public:
    
    void add(Key k, T value){
        map[k] = map[k] + value;
    }
    
    void insert(Key k, T value){
        map[k] = value;
    }
    
    T get(Key k){
        return map[k];
    }
    
    void del(Key k){
        map.erase(k);
    }
    
    int get_size(){
        return map.size();
    }
    
    std::map<Key,T> get_map(){
        return map;
    }
    
    Counter(){};
    
    Counter(std::string str){
        name = str;
    }
    
    friend std::ostream& operator<<(std::ostream& stream, const Counter counter){
        //get total
        T total = 0;
        for(auto m : counter.map){
            total += m.second;
        }

        // names
        for(auto m : counter.map){
            stream << m.first << ",";
        }
        stream << std::endl;
        
        // values
        for(auto m : counter.map){
            stream << m.second << ",";
        }
        stream << std::endl;
        
        // %
        for(auto m : counter.map){
            stream << (float)m.second/total*100  << ",";
        }
        stream << std::endl;
        
        return stream;
    }
    
};



// specialisation of the constant module
template <class Key, class T>
class ConstantModule<Counter<Key,T>> : public Constant<Counter<Key,T>> {
public:
    //additional output ports
    
    OutputPort<Key, Stream> keys;
    OutputPort<T, Stream> values;

    
    // inherite constructors
    using Constant<Counter<Key,T>>::Constant;
    
    void compute(){
        // inherite compute form parent class
        Constant<Counter<Key,T>>::compute();
        
        std::map<Key,T> m = Constant<Counter<Key,T>>::v.get_map();
        
        for(auto it: m){
            keys.send(it.first);
            values.send(it.second);
        }
        
    }
    
};

#endif /* counter_hpp */
