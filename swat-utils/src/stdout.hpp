#ifndef stdout_hpp
#define stdout_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>

#include <iostream>
#include <mutex>

template <class T, data_mode_t M>
class Stdout: virtual public Module{
private:
    static int mutex;
};

template <class T>
class Stdout<T, Value>: virtual public Module{
    std::string prefix = "";
public:
    // input ports
    InputPort<T, Value> data;
    
    Stdout(){};
    Stdout(std::string str){
        prefix = str;
    }
    
    void compute(){
        T value = data.get();
        lock.lock();
        std::cout << prefix << value << std::endl;
        lock.unlock();
    };
};



template <class T>
class Stdout<T, Stream>: virtual public Module{
public:
    // input ports
    InputPort<T, Stream> data_stream;
    
    void compute(){
        while(true){
            try {
                T value = data_stream.recv();
                lock.lock();
                std::cout << value << std::endl;
                lock.unlock();
            } catch (end_of_data) {
                break;
            }
        }
    }
};
#endif /* stdout_hpp */
