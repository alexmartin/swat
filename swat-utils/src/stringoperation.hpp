#ifndef stringoperation_hpp
#define stringoperation_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>

#include <string>

class RemoveFirstChar: virtual public Module {
private:
    int size;
    
public:

    InputPort<std::string, Stream> input_stream;
    OutputPort<std::string, Stream> output_stream;
    
    RemoveFirstChar(int size);
    
    void compute();
    
};


class StringToFloat: virtual public Module {

public:
    InputPort<std::string, Stream> string_stream;
    OutputPort<float, Stream> float_stream;
    
    void compute();
    
};

class StringToUint64: virtual public Module {
    
public:
    InputPort<std::string, Stream> string_stream;
    OutputPort<uint64_t, Stream> ull_stream;
    
    void compute();
    
};



class StringSplitter: virtual public Module {
    
public:
    InputPort<std::string, Value> i_string_stream;
    InputPort<char, Value> splitter;

    OutputPort<std::string, Stream> o_string_stream;
    
    void compute();
    
};



#endif /* stringoperation_hpp */
