#include "processes-analysis.hpp"

#include "type-event.hpp"

#include <list>
#include <map>


//struct for cpus
typedef struct cpu {
    uint64_t last_ts; // last ts for the cpu
    //uint64_t first_ts; // ts of the first event for this cpu
    uint64_t current_sw_prod = -1;
    std::string current_sw_name = "";
    std::list<EventPunctual> buffer;
    std::list<EventState> buffer_state;
} cpu_t;

// struct for producers
typedef struct producer{
    state_t curent_state = UNKNOWN;
    std::string name = "";
    std::string syscall = "";
    std::list<EventState> buffer;
} producer_t;


void ProcessAnalysis::compute(){
    
    // construct structs for cpus and producer info
    std::map<uint64_t, cpu_t> cpu_dict;
    std::map<uint64_t,cpu_t>::iterator cpu_it;

    std::map<uint64_t, producer_t> producer_dict;
    std::map<uint64_t, producer_t>::iterator producer_it;
    
    std::map<uint64_t, std::list<uint64_t>> process_tree; // pid -> p_pid
    
    uint64_t first_timestamp;
    
    EventPunctual ep;
    
    try {
        ep = event_stream.recv(); // get first
        first_timestamp = ep.timestamp;
    } catch (end_of_data) {
        // if stream is empty, dont compute anything
        event_state_stream.close();
        event_punctual_stream.close();
        return;
    }
    
    while (true) {


        // sched fork: save parent pid for new processes
        if( ep.type == "sched_process_fork"){
            uint64_t parent_prod = std::stoi(ep.data["parent_tid"]);
            uint64_t child_prod = std::stoi(ep.data["child_tid"]);
            // save link pid->ppid
            process_tree[child_prod].push_back(parent_prod);
        }
        
        if( ep.type == "sched_process_exec"){
            producer_dict[std::stoi(ep.data["tid"])].name = ep.data["filename"];
        }

        
        // get cpu id
        uint64_t cpu_id = ep.cpu;
        // get cpu object or create it if not exists
        cpu_t * cpu = nullptr;
        
        // check if current cpu exists
        cpu_it = cpu_dict.find(cpu_id);
        if (cpu_it == cpu_dict.end()){
            cpu_t new_cpu;
            new_cpu.last_ts = first_timestamp;
            cpu_dict[cpu_id] = new_cpu;
        }
        cpu = &cpu_dict[cpu_id];
        
        // get current producer for the cpu
        uint64_t current_prod_id = cpu->current_sw_prod;
        
        // if current producer is undefined (-1), buffer event in the cpu buffer
        if(current_prod_id == -1){
            cpu->buffer.push_back(ep);
        }else{
            ep.tid = current_prod_id;
            ep.p_tid = process_tree[current_prod_id].back();
            ep.procname = producer_dict[current_prod_id].name;
            // send updated data event punctual
            event_punctual_stream.send(ep);
        }
        
        producer_t * producer = nullptr;
        
        if( ep.type == "sched_switch"){


            //TODO: fix perf
            int prev_tid = std::stoi(ep.data["prev_tid"]);
            if (producer_dict[prev_tid].name.size() == 0 ){
                producer_dict[prev_tid].name = ep.data["prev_comm"];
            }
            producer_dict[std::stoi(ep.data["next_tid"])].name = ep.data["next_comm"];
            
            if(current_prod_id == -1){
                current_prod_id = std::stoi(ep.data["prev_tid"]);
                //producer_dict[current_prod_id].name = ep.data["prev_comm"];
                // unbuffer events from current cpu and send them
                for( auto e: cpu->buffer ){
                    e.tid = current_prod_id;
                    e.p_tid = process_tree[current_prod_id].back();
                    e.procname = producer_dict[current_prod_id].name;
                    event_punctual_stream.send(e);
                }
                cpu->buffer.clear();
                // unbuffer events state from current cpu and send them
                for( auto e: cpu->buffer_state ){
                    e.tid = current_prod_id;
                    e.p_tid = process_tree[current_prod_id].back();
                    e.procname = ep.data["prev_comm"];
                    event_state_stream.send(e);
                }
                cpu->buffer_state.clear();
            }
            
            // get current state of the current producer if exists or create it else
            producer_it = producer_dict.find(current_prod_id);
            if (producer_it == producer_dict.end()){
                producer_t new_prod;
                new_prod.name = ep.data["prev_comm"];
                producer_dict[current_prod_id] = new_prod;
            }
            producer = &producer_dict[current_prod_id];
            
            //create state that just ends
            EventState es;
            es.timestamp = cpu->last_ts;
            es.cpu = cpu_id;
            es.tid = current_prod_id;
            es.p_tid = process_tree[current_prod_id].back();
            es.procname = producer_dict[current_prod_id].name;
            es.state = producer->curent_state;
            if(es.state == KERNEL){
                es.type = producer->syscall;
            }else{
                es.type = "";
            }
            es.end_timestamp = ep.timestamp;
            
            // update cpu context
            cpu->current_sw_prod = std::stoi(ep.data["next_tid"]);
            cpu->current_sw_name = ep.data["next_comm"];
            cpu->last_ts = ep.timestamp;
            
            //buffer if state is unknonw, unless producer is kernel process
            if(es.state == UNKNOWN){
                // get the string of the producer
                std::string prod_name = producer_dict[current_prod_id].name;
                if(prod_name.find("kworker") == 0 or \
                   prod_name.find("kswapd0") == 0 or \
                   prod_name.find("ksoftirqd") == 0 or \
                   prod_name.find("khungtaskd") == 0 or \
                   prod_name.find("watchdog") == 0 or \
                   prod_name.find("migration") == 0 or \
                   prod_name.find("jbd2") == 0 or \
                   prod_name.find("mmcqd") == 0 or \
                   prod_name.find("rcu_sched") == 0){
                    // assign state for special producers
                    producer_dict[current_prod_id].curent_state = KERNEL;
                    if(producer_dict[current_prod_id].name.size() == 0){
                        producer_dict[current_prod_id].name = ep.data["prev_comm"];
                    }
                    es.state = KERNEL;
                    event_state_stream.send(es);
                }else if(prod_name.find("swapper") == 0){
                    //swapper is the idle process
                    producer_dict[current_prod_id].curent_state = IDLE;
                    if(producer_dict[current_prod_id].name.size() == 0){
                        producer_dict[current_prod_id].name = ep.data["prev_comm"];
                    }
                    es.state = IDLE;
                    event_state_stream.send(es);
                }else{
                    // buffer event in producer buffer
                    producer_dict[current_prod_id].buffer.push_back(es);
                }
            }else{
                event_state_stream.send(es);
            }
            
        }else if (ep.type.find("syscall_entry") ==0 ){
            //create state that just ends
            EventState es;
            es.timestamp = cpu->last_ts;
            es.cpu = cpu_id;
            es.tid = current_prod_id;
            es.p_tid = process_tree[current_prod_id].back();
            es.procname = producer_dict[current_prod_id].name;
            es.type = "";
            es.state = USER;
            es.end_timestamp = ep.timestamp;
            
            // update cpu and producer states
            cpu->last_ts = ep.timestamp;
            
            if(current_prod_id == -1){
                cpu->buffer_state.push_back(es);
            }else{
                producer_it = producer_dict.find(current_prod_id);
                if (producer_it == producer_dict.end()){
                    producer_t new_prod;
                    new_prod.name = cpu->current_sw_name;
                    producer_dict[current_prod_id] = new_prod;
                }
                producer = &producer_dict[current_prod_id];
                producer->curent_state = KERNEL;
                producer->syscall = ep.type.substr(14);
                
                // if there is states pending in the current producer buffer, flush them
                for( auto e: producer->buffer ){
                    e.state = USER;
                    e.procname = producer->name;
                    event_state_stream.send(e);
                }
                //clear buffer
                producer->buffer.clear();
                
                event_state_stream.send(es);

            }

            
        }else if (ep.type.find("syscall_exit") == 0 ){
            //create state that just ends
            EventState es;
            es.timestamp = cpu->last_ts;
            es.cpu = cpu_id;
            es.tid = current_prod_id;
            es.p_tid = process_tree[current_prod_id].back();
            es.procname = producer_dict[current_prod_id].name;
            es.type = ep.type.substr(13);
            es.state = KERNEL;
            es.end_timestamp = ep.timestamp;
            
            // update cpu and producer states
            cpu->last_ts = ep.timestamp;
            
            if(current_prod_id == -1){
                cpu->buffer_state.push_back(es);
            }else{
                producer_it = producer_dict.find(current_prod_id);
                if (producer_it == producer_dict.end()){
                    producer_t new_prod;
                    new_prod.name = cpu->current_sw_name;
                    producer_dict[current_prod_id] = new_prod;
                }
                producer = &producer_dict[current_prod_id];
                producer->curent_state = USER;
                
                // if there is states pending in the current producer buffer, flush them
                for( auto e: producer->buffer ){
                    e.state = KERNEL;
                    e.procname = producer->name;
                    event_state_stream.send(e);
                }
                //clear buffer
                producer->buffer.clear();
                
                event_state_stream.send(es);
                
            }
        }
        
        // get next event
        try {
            ep = event_stream.recv();
        } catch (end_of_data) {
            break;
        }
    }
    
    // close last states events
    for(auto cpu: cpu_dict){
        EventState es;
        es.timestamp = cpu.second.last_ts;
        es.cpu = cpu.first;
        es.tid = cpu.second.current_sw_prod;
        es.p_tid = process_tree[es.tid].back();
        es.procname = producer_dict[es.tid].name;
        es.type = "";
        es.state = producer_dict[es.tid].curent_state;
        es.end_timestamp = ep.timestamp;
        
        event_state_stream.send(es);

    }
    
    event_state_stream.close();
    event_punctual_stream.close();
    
    
}
