#include "states-duration.hpp"


void StateDuration::compute(){
    
    EventState es;
    
    Counter<state_t, uint64_t> durations;
    
    while (true) {
        try {
            es = states_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
        durations.add(es.state, es.end_timestamp-es.timestamp);
    
    }
    
    states_duration.set(durations);
    
}
