#include "module-category.hpp"

#include "type-event.hpp"
#include "type-trace.hpp"

#include <swat/utils/counter.hpp>

#include <regex>

void Category::compute(){
    
    //create the counter for event type families
    Counter<std::string, uint64_t> count;
    
    //static map of events
    std::map<std::string, std::string> cat;

    // memory
    cat["mm"] = cat["kmem"] = "memory";
    // system
    cat["rcu"] = cat["rpm"] = cat["workqueue"] = cat["signal"] = cat["sched"] = cat["module"] = cat["irq"] = cat["lttng"] = cat["regulator"] = cat["regmap"] = cat["regcache"] = cat["random"] = cat["console"] = cat["gpio"] = "system";
    // network
    cat["udp"] = cat["rcp"] = cat["sock"] = cat["skb"] = cat["net"] = cat["netif"] = cat["napi"] = "network";
    // processor
    cat["timer"] = cat["hrtimer"] = cat["itimer"] = cat["power"] = cat["softirq"] = "processor";
    // disk
    cat["ext4"] = cat["mmc"] = cat["scsi"] = cat["jbd2"] = cat["block"] = "disk";
    // graphic
    cat["v4l2"] = cat["snd"] = "graphic";
    
    
    while(true){
        
        EventPunctual ep;
        
        try {
            ep = event_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
//        //skip non kernel events
//        if(ep.category != KERNEL){
//            continue;
//        }
        
        // use regex to get the prefix of
        std::string s (ep.type);
        std::smatch m;
        std::regex e ("_");
        std::regex_search (s, m, e);
        
        if (m.ready() and cat[m.prefix().str()].length() > 0 ) {
            // match prefix to family and
            count.add(std::string(cat[m.prefix().str()]), 1);
        }else{
            //TODO: do something ? all event for user trace will pass here
            //std::cout << m.prefix() << std::endl;
        }
    }

    // set out value for the counter
    category_counter.set(count);
}
