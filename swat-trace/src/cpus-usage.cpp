#include "cpus-usage.hpp"

void CPUsUsage::compute(){
    
    EventState es;
    
    Counter<uint64_t, uint64_t> durations("CPU usage");
    
    while (true) {
        
        try {
            es = stream_event.recv();
        } catch (end_of_data) {
            break;
        }
        
        //std::cout << es.end_timestamp - es.timestamp << std::endl;

        durations.add(es.cpu, es.end_timestamp-es.timestamp);
        
    }
    
    cpus_counters.set(durations);

    
}
