#ifndef producer_filter_hpp
#define producer_filter_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"

#include <string>
#include <list>
#include <algorithm>


// filter subprocesses by process name
template <class T>
class ProcessFilter: virtual public Module {
private:
    std::string procname_filer;
public:
    InputPort<T, Stream> input_stream;
    
    OutputPort<T, Stream> matched_events;
    OutputPort<T, Stream> discarded_events;
    
    void compute(){
        
        T e;
        
        std::list<uint64_t> tid_list;
        
        bool find_proc_name = false;
        
        while (true) {
            
            try {
                e = input_stream.recv();
            } catch (end_of_data) {
                break;
            }

            //std::cout << e << std::endl;

            //do only once
            if (not find_proc_name and e.procname == procname_filer) {
                tid_list.push_back(e.tid);
                find_proc_name = true;
            }
            
            bool found = (std::find(tid_list.begin(), tid_list.end(), e.tid) != tid_list.end());
            
            // check if event is subevent
            if (not found and (std::find(tid_list.begin(), tid_list.end(), e.p_tid) != tid_list.end())) {
                tid_list.push_back(e.tid);
                found = true;
            }
            
            if(found){
                matched_events.send(e);
            }else{
                discarded_events.send(e);
            }
            
        }
        
        matched_events.close();
        discarded_events.close();
        
    }
    
    ProcessFilter(std::string procname){
        procname_filer = procname;
    }
};

#endif /* producer_filter_hpp */
