#ifndef event_hpp
#define event_hpp

#include <swat/core/type.hpp>

#include <map>
#include <iostream>

// category of event
typedef enum {PUNCTUAL, STATE, LINK, VALUE} caterory_t;

// state type of state category
typedef enum {UNKNOWN=0, USER=1, KERNEL=2, IDLE=3, IRQ=4} state_t;

// union for specific data map
union specific_data {
    int64_t value;
    uint64_t uvalue;
    int str_id;
};

typedef enum {INT, UINT, STR} data_type_t;

class Event: public GenericType{
public:
    int64_t timestamp = 0;
    int64_t cpu = -1;  //hardware producer (#cpu)
    int64_t tid = -1; // software producer (pid)
    
    // parent producers
    int64_t p_cpu = -1;  //hardware producer (#cpu)
    int64_t p_tid = -1; // software producer (pid)
    
    // todo check if this decrease perfs 
    std::string procname; // process name
    
    std::string type; // event type
    caterory_t category;
    
    // map for event specific data
    std::map<std::string, std::string> data;
    
};


class EventPunctual : public Event{
public:
    
    EventPunctual(){
        category = PUNCTUAL;
    }
    
    friend std::ostream& operator<<(std::ostream& stream, const EventPunctual ep);
};


class EventState: public Event{
public:
    long end_timestamp;
    state_t state;
    
    EventState(){
        category = STATE;
    }

    long get_duration();
    
    friend std::ostream& operator<<(std::ostream& stream, const EventState es);
};

class EventLink: public Event{
public:
    long end_timestamp;
    
    EventLink(){
        category = LINK;
    }
    
    friend std::ostream& operator<<(std::ostream& stream, const EventLink el);
};

template <class T>
class EventValue: public Event{
public:
    T value;
    
    EventValue(){
        category = VALUE;
    }
    
    friend std::ostream& operator<<(std::ostream& stream, const EventValue ev){
        stream << "EV:" << ev.timestamp << ":[" << ev.cpu << ":" << ev.tid << "]" << ev.type << " = " << ev.value;
        return stream;
    };
};

#endif /* event_hpp */
