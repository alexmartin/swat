#ifndef addition_h
#define addition_h

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"

template <class T>
class EventValueStreamAddition: public Module{
public:
    InputPort<EventValue<T>, Stream> data_stream;
    OutputPort<T, Value> addition_value;
    
    void compute(){
        EventValue<T> ev;
        T res = 0; // TODO: use initialiser
        while (true) {
            try {
                ev = data_stream.recv();
            } catch (end_of_data) {
                break;
            }
            
            res += ev.value;
            
        }
        
        addition_value.set(res);
        
    }
};


#endif /* addition_h */
