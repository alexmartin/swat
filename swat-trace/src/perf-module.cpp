#include "perf-module.hpp"

void PerfNormalization::compute(){
    
    EventPunctual ep;
    
    // all counters are CPU bounded, store values in maps
    std::map<int64_t, uint64_t> m_instr;
    std::map<int64_t, uint64_t> m_loads;
    std::map<int64_t, uint64_t> m_stores;
    
    uint64_t instr;
    uint64_t loads;
    uint64_t stores;
    
    while (true) {
        
        try {
            ep = input_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
        instr = m_instr[ep.cpu];
        loads = m_loads[ep.cpu];
        stores = m_stores[ep.cpu];
        
        m_instr[ep.cpu] = std::stoull(ep.data["perf_cpu_instructions"]);
        m_loads[ep.cpu] = std::stoull(ep.data["perf_cpu_L1_dcache_loads"]);
        m_stores[ep.cpu] = std::stoull(ep.data["perf_cpu_L1_dcache_stores"]);

        ep.data["perf_cpu_instructions"] = std::to_string(m_instr[ep.cpu] - instr);
        ep.data["perf_cpu_L1_dcache_loads"] = std::to_string(m_loads[ep.cpu] - loads);
        ep.data["perf_cpu_L1_dcache_stores"] = std::to_string(m_stores[ep.cpu] - stores);
        
        
        output_stream.send(ep);
        
    }
    
    output_stream.close();
    
}


void PerfValues::compute(){
    
    EventPunctual ep;
    EventValue<uint64_t> mem_instr_ev;
    EventValue<uint64_t> cpu_instr_ev;
    
    while (true) {
        
        try {
            ep = input_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
        cpu_instr_ev.timestamp = ep.timestamp;
        cpu_instr_ev.cpu = ep.cpu;
        cpu_instr_ev.type = "";
        cpu_instr_ev.value = std::stoull(ep.data["perf_cpu_instructions"]);
        
        mem_instr_ev.timestamp = ep.timestamp;
        mem_instr_ev.cpu = ep.cpu;
        mem_instr_ev.type = "";
        mem_instr_ev.value = std::stoull(ep.data["perf_cpu_L1_dcache_loads"]) + std::stoull(ep.data["perf_cpu_L1_dcache_stores"]);
        
        mem_instr.send(mem_instr_ev);
        cpu_instr.send(cpu_instr_ev);
        
    }
    
    mem_instr.close();
    cpu_instr.close();
    
    
}
