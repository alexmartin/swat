#include "state-filter.hpp"

StateFilter::StateFilter(state_t state){
    state_filter = state;
}

void StateFilter::compute(){

    EventState es;
    
    while (true) {
        try {
            es = input_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
        if (es.state == state_filter) {
            matched_events.send(es);
        }else{
            discarded_events.send(es);
        }

    }
    
    matched_events.close();
    discarded_events.close();
}
