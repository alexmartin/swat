#include "compute-duration.hpp"

void ComputeDuration::compute(){
    
    EventState es;
    
    uint64_t beg = 0;
    uint64_t end = 0;
    
    while (true) {

        try {
            es = states_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
        if(beg == 0){
            beg = es.timestamp;
        }  
        end = es.end_timestamp;   
    
    }
    
    duration.set(end-beg);
    
}
