#ifndef type_trace_hpp
#define type_trace_hpp

// trace metadata

// duration, date of trace, number of raw events.
// mapping id/names ?
// use trace metadata for modules that uses events, and update metadata, ie. when constructing states ?

#include <swat/core/type.hpp>

#include <swat/utils/constant.hpp>

#include <list>
#include <map>

class Trace: public GenericType {
    
public:
    // number of raw event
    uint64_t number_pevents = 0;
    uint64_t number_sevents = 0;
    uint64_t number_levents = 0;
    uint64_t number_vevents = 0;
    
    uint64_t trace_date = 0;
    uint64_t trace_duration = 0;
    
    // name of the trace
    std::string name;
    
    // map for process tree : pid->children_id_list
    std::map<uint64_t, std::list<uint64_t>> process_tree;
    
    //map for process name pid->name
    std::map<uint64_t, std::string> process_name;

    friend std::ostream& operator<<(std::ostream& stream, const Trace s);
};


// specialisation of the constant module
//TODO
template<>
class ConstantModule<Trace> : public Constant<Trace> {
public:
    //additional output ports
    OutputPort<uint64_t, Value> punctual_events_number;
    OutputPort<uint64_t, Value> state_events_number;
    OutputPort<uint64_t, Value> link_events_number;
    OutputPort<uint64_t, Value> value_events_number;

    OutputPort<uint64_t, Value> date;
    OutputPort<uint64_t, Value> duration;
    
    OutputPort<std::string, Value> name;

    // inherite constructors
    using Constant<Trace>::Constant;
    
    void compute(){
        punctual_events_number.set(v.number_pevents);
        state_events_number.set(v.number_sevents);
        link_events_number.set(v.number_levents);
        value_events_number.set(v.number_vevents);
        
        date.set(v.trace_date);
        duration.set(v.trace_duration);
        
        name.set(v.name);
    }
    
};

#endif /* type_trace_hpp */
