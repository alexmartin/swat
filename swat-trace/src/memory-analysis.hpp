#ifndef memory_analysis_hpp
#define memory_analysis_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include <swat/utils/counter.hpp>

#include "type-event.hpp"

class MemoryAnalysis: public Module{
public:
    InputPort<EventPunctual, Stream> event_stream;
    
    OutputPort<EventValue<int64_t>, Stream> values_stream;
    
    OutputPort<int64_t, Value> max_allocated_memory;
    OutputPort<int64_t, Value> useless_free_calls;
    OutputPort<int64_t, Value> realloc_calls_as_malloc;
    
    OutputPort<Counter<std::string, int64_t>, Value> function_calls;
    OutputPort<Counter<int64_t, int64_t>, Value> allocations_size;

    void compute();
    
};

#endif /* memory_analysis_hpp */
