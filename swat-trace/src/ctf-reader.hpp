#ifndef ctf_reader_hpp
#define ctf_reader_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"


class CTFReader: public Module {
private:
    bool use_user_trace = false;

public:
    InputPort<std::string, Value> filename;
    
    OutputPort<EventPunctual, Stream> events_stream;
    //OutputPort<Trace, Value> trace_metadata;
    OutputPort<uint64_t, Value> events_number;
    OutputPort<uint64_t, Value> duration;

    CTFReader();
    CTFReader(bool use_user_trace);
    
    void compute();
    
};

#endif /* ctf_reader_hpp */
