#ifndef countermodule_hpp
#define countermodule_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"

template <class T>
class CounterModule: public Module{
public:
    InputPort<T, Stream> data_stream;
    OutputPort<uint64_t, Value> number;
    
    void compute(){
        T e;
        uint64_t res = 0;
        while (true) {
            
            try {
                e = data_stream.recv();
            } catch (end_of_data) {
                break;
            }
            
            res += 1;
            
        }
        
        number.set(res);
        
    }
};

#endif /* countermodule_hpp */
