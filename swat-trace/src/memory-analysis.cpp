#include "memory-analysis.hpp"

#include <swat/utils/counter.hpp>

void MemoryAnalysis::compute(){
    
    EventPunctual ep;
    
    EventValue<int64_t> ev;
    
    Counter<std::string, int64_t> memory_stats;
    Counter<int64_t, int64_t> alocations_sizes;
    Counter<int64_t, int64_t> allocated_by_process;
    Counter<std::string, int64_t> allocations;
    
    int64_t allocated = 0;
    int64_t old_allocated = 0;
    int64_t max_allocated = 0;
    int64_t realloc_as_malloc = 0;
    int64_t useless_free = 0;
    
    //TODO: send values per process ?
    
    while (true) {
        try {
            ep = event_stream.recv();
        } catch (end_of_data) {
            break;
        }
        
        if (ep.type == "lttng_ust_libc:malloc") {
            
            memory_stats.add("malloc", 1);
            int64_t size = std::stoull(ep.data["size"]);
            allocations.insert(ep.data["ptr"], size);
            allocated += size;
            allocated_by_process.add(ep.tid, size);
            alocations_sizes.add(size, 1);
            
            
        }else if (ep.type == "lttng_ust_libc:free") {
            
            memory_stats.add("free", 1);
            
            std::string ptr = ep.data["ptr"];
            
            if (ptr != "0") {
                int64_t size = allocations.get(ptr);
                allocated -= size;
                allocated_by_process.add(ep.tid, -size);
                allocations.del(ptr);
            }else{
                useless_free += 1;
            }
            
        }else if (ep.type == "lttng_ust_libc:calloc") {
            
            memory_stats.add("calloc", 1);
            
            std::string ptr = ep.data["ptr"];
            
            int64_t size = std::stoi(ep.data["nmemb"]) * std::stoull(ep.data["size"]);
            allocations.insert(ptr, size);
            allocated += size;
            allocated_by_process.add(ep.tid, size);
            
        }else if (ep.type == "lttng_ust_libc:realloc") {
            
            memory_stats.add("realloc", 1);
            
            std::string in_ptr = ep.data["in_ptr"];
            
            if (in_ptr != "0") {
                int64_t oldsize = allocations.get(in_ptr);
                allocated -= oldsize;
                allocated_by_process.add(ep.tid, -oldsize);
                allocations.del(in_ptr);
            }else{
                realloc_as_malloc += 1;
            }
            
            int64_t size = std::stoull(ep.data["size"]);
            allocated += size;
            allocated_by_process.add(ep.tid, size);
            allocations.insert(ep.data["ptr"], size);
            alocations_sizes.add(size, 1);
            
        }else if (ep.type == "sched_process_exit"){
            
            int64_t size = allocated_by_process.get(ep.tid);
            allocated -= size;
            
        }
        
        if(allocated != old_allocated){
            ev.timestamp = ep.timestamp;
            ev.value = allocated;
            ev.type = "memory";
            
            if(allocated > max_allocated){
                max_allocated = allocated;
            }
            
            values_stream.send(ev);
        }
        
        old_allocated = allocated;
        
    }
    
    values_stream.close();
    
    max_allocated_memory.set(max_allocated);
    useless_free_calls.set(useless_free);
    realloc_calls_as_malloc.set(realloc_as_malloc);
    function_calls.set(memory_stats);
    allocations_size.set(alocations_sizes);
    
}
