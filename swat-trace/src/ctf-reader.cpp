#include "ctf-reader.hpp"

#include "type-event.hpp"
#include "type-trace.hpp"

// babeltrace generic header for trace management
#include <babeltrace/babeltrace.h>
#include <babeltrace/ctf/iterator.h>
#include <babeltrace/ctf/events.h>

#include <iostream>
#include <sstream>


CTFReader::CTFReader(){
    
}

CTFReader::CTFReader(bool use_user_trace){
    this->use_user_trace = use_user_trace;
}


bool has_suffix(const std::string &str, const std::string &suffix)
{
    return str.size() >= suffix.size() &&
    str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

void CTFReader::compute(){
    
    std::string path = filename.get();
    
    struct bt_context *ctx = bt_context_create();
    
    // add kernel part of the trace
    int th_k = bt_context_add_trace(ctx, (path+"/kernel").c_str(), "ctf", nullptr, nullptr, nullptr);
    //get timestamps begin/end kernel
    int64_t ts_beg_k = (int64_t) bt_trace_handle_get_timestamp_begin(ctx, th_k, BT_CLOCK_REAL); //in nanosecond
    int64_t ts_end_k = (int64_t) bt_trace_handle_get_timestamp_end(ctx, th_k, BT_CLOCK_REAL); //in nanosecond
    
    int64_t ts_beg, ts_end;
    
    if(use_user_trace){
        //int th_u = bt_context_add_trace(ctx, (path+"/ust/uid/10469/64-bit/").c_str(), "ctf", nullptr, nullptr, nullptr);
        int th_u = bt_context_add_trace(ctx, (path+"/ust/uid/1000/64-bit/").c_str(), "ctf", nullptr, nullptr, nullptr);
        //get timestamps begin/end kernel
        int64_t ts_beg_u = (int64_t) bt_trace_handle_get_timestamp_begin(ctx, th_u, BT_CLOCK_REAL); //in nanosecond;
        int64_t ts_end_u = (int64_t) bt_trace_handle_get_timestamp_end(ctx, th_u, BT_CLOCK_REAL); //in nanosecond;
        
        ts_beg = std::min(ts_beg_k, ts_beg_u);
        ts_end = std::max(ts_end_k, ts_end_u);
        
    }else{
        ts_beg = ts_beg_k;
        ts_end = ts_end_k;
    }
    
    
    
    
    // store trace creation
    //trace.trace_date = ts_beg;
    
    // store trace turation
    duration.set(ts_end - ts_beg);
    
    unsigned long long n_events = 0;
    
    // get iterator of events
    struct bt_ctf_iter *envents_iter = nullptr;
    envents_iter = bt_ctf_iter_create(ctx, nullptr, nullptr);

    // TODO: should be checked before each events...
//    // check no events were discarded during tracing
//    if(bt_ctf_get_lost_events_count(envents_iter) != 0){
//        return;
//    }
    
    //event pointer
    struct bt_ctf_event *ev_ptr = nullptr;
    
    int64_t first_ts = 0;
    
    // iterate over all events in the trace
    while(true){

        // get event pointer
        ev_ptr = bt_ctf_iter_read_event(envents_iter);
        // break if end of events
        if(ev_ptr == nullptr){
            break;
        }
        
        // update global events counter
        n_events++;

        // create a new punctual event
        EventPunctual e;
        
        // retreive the timestamp
        int64_t ts = (int64_t) bt_ctf_get_timestamp(ev_ptr);
        
        
        //normalize timestamp
        if(first_ts == 0){
            first_ts = ts;
        }
        e.timestamp = ts - first_ts;

        // TODO: check lifetime of this pointer OR add hash map on the trace metadata produced
        e.type = bt_ctf_event_name(ev_ptr);

        // get the scope for event
        const struct bt_definition* scope = bt_ctf_get_top_level_scope(ev_ptr, BT_STREAM_PACKET_CONTEXT);
        // get field
        const struct bt_definition* field = bt_ctf_get_field(ev_ptr, scope, "cpu_id");
        e.cpu = bt_ctf_get_uint64(field);
        
        
        //get all specific event data
        // python: event_d = {key: event[key] for key in event.field_list_with_scope(babeltrace.CTFScope.EVENT_FIELDS)}
        
        auto top_levels = {BT_EVENT_FIELDS, BT_STREAM_EVENT_CONTEXT};
        
        for (auto level: top_levels){
            
            scope = bt_ctf_get_top_level_scope(ev_ptr, level);
            
            unsigned int count=0;
            struct bt_definition const * const * list;
            int ret = bt_ctf_get_field_list(ev_ptr, scope, &list, &count);
            if (ret >= 0){
                
                while(count > 0){
                    
                    //ostring for result value
                    std::ostringstream oss;
                    
                    struct bt_definition const * field = list[--count];
                    
                    //get field name
                    std::string name = std::string(bt_ctf_field_name(field));
                    
                    // get field type
                    switch (bt_ctf_field_type(bt_ctf_get_decl_from_def(field))) {
                            
                        case CTF_TYPE_INTEGER:
                            if(bt_ctf_get_int_signedness(bt_ctf_get_decl_from_def(field))){ // signed
                                oss << bt_ctf_get_int64(field);
                            }else{
                                oss << bt_ctf_get_uint64(field);
                            }
                            // removed: not all trace used this context info
                            //                        if(not(name.compare("tid") and name.compare("parent_tid"))){
                            //                            hastid = true;
                            //                            e.tid = bt_ctf_get_int64(field);
                            //                        }
                            break;
                            
                        case CTF_TYPE_FLOAT:
                            //TODO: ?
                            break;
                            
                        case CTF_TYPE_STRING:
                            oss << bt_ctf_get_string(field);
                            break;
                            
                        case CTF_TYPE_ARRAY:
                            //TODO: manage compound types using recursive functions ? or pass ctf address of data
                            
                            //TODO: ugly quick fix, improve
                            if(has_suffix(name,"comm")){
                                oss << bt_ctf_get_char_array(field);
                            }else{
                                //std::cout << name << " DISCARDED" << std::endl;
                            }
                            break;
                            
                        default:
                            //TODO: ?
                            //std::cout << "#" << bt_ctf_field_type(bt_ctf_get_decl_from_def(field));
                            break;
                    }
                    
                    
                    
                    e.data[name] = oss.str();
                }
                //            if( not hastid){
                //                std::cout << "#####" << e.type << std::endl;
                //            }
            }
        
        }
        
        // send event
        events_stream.send(e);
        
        //move iterator to the next event
        struct bt_iter *iter = bt_ctf_get_iter(envents_iter);
        
        bt_iter_next(iter);


    }
    
    
    events_number.set(n_events);
    
    //close event stream
    events_stream.close();
    
    //free the event iterator
    bt_ctf_iter_destroy(envents_iter);

}
