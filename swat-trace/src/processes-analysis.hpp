#ifndef processes_analysis_hpp
#define processes_analysis_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>

#include "type-event.hpp"
#include "type-trace.hpp"

// reconstruct states user/kernel
// add info about processes to trace metadata : processes names , processes tree


class ProcessAnalysis: virtual public Module{
public:
    InputPort<EventPunctual, Stream> event_stream;
    
    OutputPort<EventState, Stream> event_state_stream;
    OutputPort<EventPunctual, Stream> event_punctual_stream;
    
    void compute();
    
};

#endif /* processes_analysis_hpp */
