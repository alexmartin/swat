#ifndef gnuplot_hpp
#define gnuplot_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"

#include <cstdio>
#include <fstream>

template <class T>
class PlotGraph: public Module{
public:
    InputPort<EventValue<T>, Stream> data_stream;
    
    void compute(){
        EventValue<T> ev;
        
        std::ofstream myfile;
        myfile.open("/tmp/data.dat");
        
        
        while (true) {
            try {
                ev = data_stream.recv();
            } catch (end_of_data) {
                break;
            }
            
            myfile << ev.timestamp << '\t' << ev.value << std::endl;
            
        }
        
        system("/usr/bin/gnuplot -p -e \"plot \'/tmp/data.dat' with fsteps title 'Memory usage' \"");
        
        myfile.close();

    }
    
};




template<class T>
class PlotBoxplot: public Module{
private:
    int size;
public:
    
    InputPort<T, Stream> *data_stream;
    InputPort<std::string, Value> *data_title;

    InputPort<std::string, Value> graph_title;
    InputPort<std::string, Value> graph_path;
    
    PlotBoxplot(int size=1){
        this->size = size;
        data_stream = new InputPort<T, Stream> [size];
        data_title = new InputPort<std::string, Value> [size];
    }
    
    void compute(){
        
        std::string name = graph_title.get();
        std::string path = graph_path.get();

        
        std::string command = "/usr/bin/gnuplot -p -e \"";
        command += "set style fill solid 0.25 border -1;";
        command += "set style boxplot outliers pointtype 7;";
        command += "set style data boxplot;";
        command += "set nokey;";
        command += "set yrange[0:];";
        command += "set title '" + name + "';";
        
        //TODO: dynamic
        command += "set xtics (";
        for (int i=0; i<size; i++) {
            command += "'"+data_title[i].get()+"'"+std::to_string(i)+",";
        }
        command += ") scale 0.0;";
    
        command += "set terminal svg; set output '"+ path + name +"-boxplot.svg';";

        command += "plot ";
        
        for(int id=0; id<size; id++){
            //TODO: improve random generation file for dat
            std::string tmpname = "/tmp/plot.dat."+std::to_string(rand());
            std::ofstream myfile;
            myfile.open(tmpname, std::ios::out|std::ios::trunc);
            
            T e;
            while(true){
                try {
                    e = data_stream[id].recv();
                } catch (end_of_data) {
                    break;
                }
                
                myfile << e << std::endl;
                
            }
            
            myfile.close();
            command += "'"+tmpname + "' using ("+ std::to_string(id) +"):1,";// title '"+ data_title[id].get() +"',";
        }
        
        command += "\"";
        
        
        
        system(command.c_str());
        
    }
    
};


template<class T>
class PlotLine: public Module{
private:
    int size;
public:
    
    InputPort<T, Stream> *data_stream;
    InputPort<std::string, Value> *data_title;
    
    InputPort<std::string, Value> graph_title;
    InputPort<std::string, Value> graph_path;
    
    PlotLine(int size=1){
        this->size = size;
        data_stream = new InputPort<T, Stream> [size];
        data_title = new InputPort<std::string, Value> [size];
    }

    
    void compute(){
        
        std::string name = graph_title.get();
        std::string path = graph_path.get();
        
        std::string command = "/usr/bin/gnuplot -p -e \"";
        command += "set style data lines;";
        command += "set key box center bottom outside horizontal;";
        command += "set yrange[0:];";

        command += "set title enhanced '" + name + "';";
        
        command += "set terminal svg; set output '"+path+name+".svg';";
        
        command += "plot ";
        
        for(int id=0; id<size; id++){
            //TODO: improve random generation file for dat
            std::string tmpname = "/tmp/plot.dat."+std::to_string(rand());
            std::ofstream myfile;
            myfile.open(tmpname);
            
            T e;
            while(true){
                try {
                    e = data_stream[id].recv();
                } catch (end_of_data) {
                    break;
                }
                
                myfile << e << std::endl;
                
            }
            
            myfile.close();
            command += "'"+tmpname + "' title '"+ data_title[id].get() +"',";
        }
        
        command += "\"";
        
        
        
        system(command.c_str());
        
    }
    
};

#include "type-event.hpp"

// Specialization for EventValue plot
template<class T>
class PlotLine<EventValue<T>>: public Module{
private:
    int size;
public:
    
    InputPort<EventValue<T>, Stream> *data_stream;
    InputPort<std::string, Value> *data_title;
    
    InputPort<std::string, Value> graph_title;
    InputPort<std::string, Value> graph_path;
    
    PlotLine(int size=1){
        this->size = size;
        data_stream = new InputPort<EventValue<T>, Stream> [size];
        data_title = new InputPort<std::string, Value> [size];
    }

    
    void compute(){
        
        std::string name = graph_title.get();
        std::string path = graph_path.get();
        
        std::string command = "/usr/bin/gnuplot -p -e \"";
        command += "set style data lines;";
        command += "set key box center bottom outside horizontal;";
        command += "set title enhanced '" + name + "';";
        command += "set yrange[0:];";

        command += "set terminal svg; set output '"+path+name+".svg';";
        
        command += "plot ";
        
        for(int id=0; id<size; id++){
            //TODO: improve random generation file for dat
            std::string tmpname = "/tmp/plot.dat."+std::to_string(rand());
            std::ofstream myfile;
            myfile.open(tmpname);
            
            EventValue<T> ev;
            while(true){
                try {
                    ev = data_stream[id].recv();
                } catch (end_of_data) {
                    break;
                }
                
                myfile << ev.timestamp << '\t' << ev.value << std::endl;
                
            }
            
            myfile.close();
            command += "'"+tmpname + "' with fsteps title '"+ data_title[id].get() +"',";
        }
        
        command += "\"";
        
        
        
        system(command.c_str());
        
    }

    
};


#endif /* gnuplot_hpp */
