#ifndef states_duration_hpp
#define states_duration_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"
#include <swat/utils/counter.hpp>

class StateDuration: public Module{
    
public:
    InputPort<EventState, Stream> states_stream;
    OutputPort<Counter<state_t, uint64_t>, Value> states_duration;
    
    void compute();
    
};
#endif /* states_duration_hpp */
