#ifndef module_category_hpp
#define module_category_hpp

#include <swat/core/module.hpp>
#include <swat//core/port.hpp>

#include "type-event.hpp"

#include <swat/utils/counter.hpp>

#include <map>
#include <string>

class Category: virtual public Module{
public:
    InputPort<EventPunctual, Stream> event_stream;
    
    OutputPort<Counter<std::string, uint64_t>, Value> category_counter;
    
    void compute();
    
};

#endif /* module_category_hpp */
