#include "type-event.hpp"

#include <swat/utils/constant.hpp>

#include <iostream>

std::ostream& operator<<(std::ostream& stream, const EventPunctual ep){
    stream << "EP:" << ep.timestamp << ":[" << ep.cpu << ":" << ep.tid << "(" << ep.procname << ")" "]:" << ep.type;
    stream << " {";
    for(auto d: ep.data){
        stream << " " << d.first << ":" << d.second;
    }
    stream << " } ";
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const EventState es){
    stream << "ES:" << es.timestamp << ":[" << es.cpu << ":" << es.tid << "]" << es.type << ":" << es.state << "->" << es.end_timestamp;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const EventLink el){
    stream << "EL:" << el.timestamp << ":[" << el.cpu << ":" << el.tid << "]" << el.type << "->" << el.end_timestamp;
    return stream;
}


// specialisation of the constant module
template <>
class ConstantModule<EventPunctual> : public Constant<EventPunctual> {
public:
    //additional output ports
    OutputPort<uint64_t, Value> timestamp;
    
    // inherite constructors
    using Constant<EventPunctual>::Constant;
    
    void compute(){
        // inherite compute form parent class
        Constant<EventPunctual>::compute();
        
    }
    
};

