#ifndef perf_module_hpp
#define perf_module_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"


/*
 perf counter recorded with lttng are incremental,
 in order to be able to filter processes but kep value
 */

class PerfNormalization: public Module{
public:
    InputPort<EventPunctual, Stream> input_stream;
    OutputPort<EventPunctual, Stream> output_stream;

    void compute();
};

// send aggregated values
class PerfValues: public Module{
public:
    InputPort<EventPunctual, Stream> input_stream;
    
    OutputPort<EventValue<uint64_t>, Stream> mem_instr;
    OutputPort<EventValue<uint64_t>, Stream> cpu_instr;

    void compute();

};

// stream event value of perf counters
class PerfValuesStream: public Module{
public:
    InputPort<EventPunctual, Stream> input_stream;
    
    OutputPort<EventValue<uint64_t>, Stream> mem_instr;
    OutputPort<EventValue<uint64_t>, Stream> cpu_instr;
    
    void compute();
    
};

#endif /* perf_module_hpp */
