#ifndef compute_duration_hpp
#define compute_duration_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"

class ComputeDuration: public Module{
    
public:
    InputPort<EventState, Stream> states_stream;
    OutputPort<uint64_t, Value> duration;
    
    void compute();
    
};
#endif /* compute_duration_hpp */
