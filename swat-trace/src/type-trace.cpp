#include "type-trace.hpp"

#include <ostream>

std::ostream& operator<<(std::ostream& stream, const Trace t){
    stream << "Trace: " << t.name << "{" << std::endl;
    stream << "\tnumber of events: " << t.number_pevents << std::endl;
    //TODO: transform en ctime
    stream << "\tdate: " << t.trace_date << std::endl;
    stream << "\tduration: " << t.trace_duration << "ns" << std::endl;
    stream << "}" << std::endl;
    return stream;
}
