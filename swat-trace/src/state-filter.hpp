#ifndef state_filter_hpp
#define state_filter_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include "type-event.hpp"

class StateFilter: public Module{
    state_t state_filter;
public:
    InputPort<EventState, Stream> input_stream;
    
    OutputPort<EventState, Stream> matched_events;
    OutputPort<EventState, Stream> discarded_events;

    void compute();
    
    StateFilter(state_t state);
    
};

#endif /* state_filter_hpp */
