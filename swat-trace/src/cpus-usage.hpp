#ifndef cpus_usage_hpp
#define cpus_usage_hpp

#include <swat/core/module.hpp>
#include <swat/core/port.hpp>

#include <swat/utils/counter.hpp>

#include "type-event.hpp"

class CPUsUsage: public Module{
public:
    InputPort<EventState, Stream> stream_event;
    OutputPort<Counter<uint64_t, uint64_t>, Value> cpus_counters;
    
    void compute();
};

#endif /* cpus_usage_hpp */
