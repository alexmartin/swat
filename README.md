# Swat

Swat is a collection of tools for trace analysis.
It is designed to be generic, thus it can be used to trace and analyze any program.

Swat is based on LTTng for trace recording and the babeltrace library for trace reading.
It provides predefined workflows to analyze recorded traces and obtain performance metrics.
Swat provides as well the ability to create your own workflows.  

# Repository organisation

This repository contains several forlders organized as follow :

## Trace recording

Folder `tracing` contains bash scripts for trace recording.
Sub-folder `script-analysis` contains the diferents configurations. 

## Swat sources

Swat library is divided into 3 parts:
- `swat-core` defines workflow mecanism, based on modules and links.
- `swat-utils` defines generic tools modules such as files, strings or stream managment.   
- `swat-trace` provides modules related to trace analysis such as CTF reading module or statistical analyses modules.

## Workflows

Folder `workflows` contains example workflows.


## Trace example

We provide in the `example-trace` an example of a previously recorded trace, on the `scimark` benchmark.
It contains the raw CTF traces (`lttng-traces`) using diferent configurations.
The `results` folder contains results about the runs (execution time, benchmark score, trace siz).

## Bash scripts

Folders `tracing-scripts` and `utils-scripts` contain bash scripts used in our example of analysis of phoronix benchmarks.



# Swat usage

## Requirements

libbabeltrace
libbabteltrace-ctf

## Build

Each part of the swat library depends on other 
swat-core -> swat-utils -> swat-trace 

In each of the 3 directories, you must run these commands :

    autoreconf
    ./configure
    make 
    sudo make install

This will install the Swat libraries onto `/usr/local/lib/swat`


In workflow directory see Makefile.

