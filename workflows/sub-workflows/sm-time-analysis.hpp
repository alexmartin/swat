#ifndef subworkflows_hpp
#define subworkflows_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>
#include <swat/core/workflow.hpp>

#include <swat/trace/gnuplot.hpp>

#include <swat/utils/streamtovalues.hpp>
#include <swat/utils/valuestostream.hpp>
#include <swat/utils/constant.hpp>
#include <swat/utils/file.hpp>
#include <swat/utils/stringoperation.hpp>
#include <swat/utils/stat.hpp>


#include "stabilitywarmupgraphs.hpp"

extern int number_of_runs;
extern std::string output_base;

// Sub workflow that compute analysis time file for one analysis
class GetValuesTime: virtual public Module {

public:
    
    InputPort<std::string, Value> ip_bench_base; // file path without "-runX"
    
    OutputPort<float, Stream> op_real_time_stream;
    OutputPort<float, Stream> op_user_time_stream;
    OutputPort<float, Stream> op_sys_time_stream;
    
    void compute(){
        
        std::string benchmark = ip_bench_base.get();
        
        Workflow w;
    
        int const num_runs = number_of_runs;
            
        ValuesToStream<std::string> *stv_time_real = new ValuesToStream<std::string> (num_runs);
        ValuesToStream<std::string> *stv_time_user = new ValuesToStream<std::string> (num_runs);
        ValuesToStream<std::string> *stv_time_sys = new ValuesToStream<std::string> (num_runs);
        w.addModule(*stv_time_real);
        w.addModule(*stv_time_user);
        w.addModule(*stv_time_sys);
        
        for(int r=0; r<num_runs; r++){
            std::string filename = benchmark+"-run"+std::to_string(r);
            
            // file name of result
            Constant<std::string> *file = new Constant<std::string>(filename);
            w.addModule(*file);
            
            // open and read file
            ReadFile<std::string> *rf = new ReadFile<std::string>;
            w.addModule(*rf);
            
            w.link(file->constant_value_out, rf->path);
            
            // split input stream into 3 ones (real, user, kerne)
            StreamToValues<std::string> *splitter = new StreamToValues<std::string> (3);
            w.addModule(*splitter);
            
            w.link(rf->content, splitter->datastream);
            
            w.link(splitter->datavalue[0], stv_time_real->data[r]);
            w.link(splitter->datavalue[1], stv_time_user->data[r]);
            w.link(splitter->datavalue[2], stv_time_sys->data[r]);
            
        }
        
        // remove starting of lines in the file
        RemoveFirstChar *remove_real = new RemoveFirstChar(5);
        RemoveFirstChar *remove_user = new RemoveFirstChar(5);
        RemoveFirstChar *remove_sys = new RemoveFirstChar(4);
        w.addModule(*remove_real);
        w.addModule(*remove_user);
        w.addModule(*remove_sys);
        
        w.link(stv_time_real->datastream, remove_real->input_stream);
        w.link(stv_time_user->datastream, remove_user->input_stream);
        w.link(stv_time_sys->datastream, remove_sys->input_stream);
        
        // convert string to float
        StringToFloat *stf_real = new StringToFloat;
        StringToFloat *stf_user = new StringToFloat;
        StringToFloat *stf_sys = new StringToFloat;
        w.addModule(*stf_real);
        w.addModule(*stf_user);
        w.addModule(*stf_sys);
        
        w.link(remove_real->output_stream, stf_real->string_stream);
        w.link(remove_user->output_stream, stf_user->string_stream);
        w.link(remove_sys->output_stream, stf_sys->string_stream);
        
        w.link_outputs(op_real_time_stream, stf_real->float_stream);
        w.link_outputs(op_user_time_stream, stf_user->float_stream);
        w.link_outputs(op_sys_time_stream, stf_sys->float_stream);
        
        w.start();
        
    }

};

// all analaysis of time files for one bench
class GetTimeAllAnalysis: virtual public Module {

public:

    InputPort<std::string, Value> bench_path;
    InputPort<std::string, Value> bench_name;
    InputPort<std::string, Value> res_path;
    
    void compute(){
        
        std::string benchmark = bench_name.get();
        std::string benchmark_path = bench_path.get();
        //std::string result_path = res_path.get();
        std::string result_path = output_base + "/time-analysis/";
        
        Workflow w;
        
        std::list<std::string> analysis_list = {"all-events","libc","perf-trace","time"};
        
        // stability modules (for the 4 analysis)
        StabilityWarmupGraphs<float> *stability_real = new StabilityWarmupGraphs<float> (4);
        StabilityWarmupGraphs<float> *stability_user = new StabilityWarmupGraphs<float> (4);
        StabilityWarmupGraphs<float> *stability_sys = new StabilityWarmupGraphs<float> (4);
        w.addModule(*stability_real);
        w.addModule(*stability_user);
        w.addModule(*stability_sys);
        
        // set title
        Constant<std::string> title_real (benchmark+"-time-real");
        Constant<std::string> title_user (benchmark+"-time-user");
        Constant<std::string> title_sys (benchmark+"-time-sys");
        w.addModule(title_real);
        w.addModule(title_user);
        w.addModule(title_sys);
        
        w.link(title_real.constant_value_out,stability_real->graph_title);
        w.link(title_user.constant_value_out,stability_user->graph_title);
        w.link(title_sys.constant_value_out,stability_sys->graph_title);
        
        
        // set dest path
        Constant<std::string> dest_path (result_path);
        
        w.addModule(dest_path);
        w.link(dest_path.constant_value_out,stability_real->graph_path);
        w.link(dest_path.constant_value_out,stability_user->graph_path);
        w.link(dest_path.constant_value_out,stability_sys->graph_path);
        
        int analysis_id = 0;
        for(std::string analysis: analysis_list){
            
            Constant<std::string> *bench_base = new Constant<std::string>(benchmark_path+benchmark+"-"+analysis);
            w.addModule(*bench_base);
            
            Constant<std::string> *analysis_name = new Constant<std::string>(analysis);
            w.addModule(*analysis_name);
            
            GetValuesTime *gt = new GetValuesTime;
            w.addModule(*gt);
            
            w.link(bench_base->constant_value_out, gt->ip_bench_base);
            
            w.link(gt->op_real_time_stream, stability_real->data_stream[analysis_id]);
            w.link(gt->op_user_time_stream, stability_user->data_stream[analysis_id]);
            w.link(gt->op_sys_time_stream, stability_sys->data_stream[analysis_id]);
            
            w.link(analysis_name->constant_value_out, stability_real->serie_name[analysis_id]);
            w.link(analysis_name->constant_value_out, stability_user->serie_name[analysis_id]);
            w.link(analysis_name->constant_value_out, stability_sys->serie_name[analysis_id]);
            
            
            // for each analysis, compute stat and sort in a file
            // compute stats
            ComputeStat<float> *real_time_stat = new ComputeStat<float>;
            ComputeStat<float> *user_time_stat = new ComputeStat<float>;
            ComputeStat<float> *sys_time_stat = new ComputeStat<float>;
            w.addModule(*real_time_stat);
            w.addModule(*user_time_stat);
            w.addModule(*sys_time_stat);
            
            w.link(gt->op_real_time_stream, real_time_stat->data_in);
            w.link(gt->op_user_time_stream, user_time_stat->data_in);
            w.link(gt->op_sys_time_stream, sys_time_stat->data_in);
            
            // save stat to file
            Constant<std::string> *path_real = new Constant<std::string> (result_path+benchmark+"-"+analysis+"-real.txt");
            Constant<std::string> *path_user = new Constant<std::string> (result_path+benchmark+"-"+analysis+"-user.txt");
            Constant<std::string> *path_sys = new Constant<std::string> (result_path+benchmark+"-"+analysis+"-sys.txt");
            w.addModule(*path_real);
            w.addModule(*path_user);
            w.addModule(*path_sys);
            
            WriteFile<Stat<float>> *write_real = new WriteFile<Stat<float>>;
            WriteFile<Stat<float>> *write_user = new WriteFile<Stat<float>>;
            WriteFile<Stat<float>> *write_sys = new WriteFile<Stat<float>>;
            w.addModule(*write_real);
            w.addModule(*write_user);
            w.addModule(*write_sys);
            
            w.link(path_real->constant_value_out, write_real->path);
            w.link(path_user->constant_value_out, write_user->path);
            w.link(path_sys->constant_value_out, write_sys->path);
            
            w.link(real_time_stat->stat, write_real->content);
            w.link(user_time_stat->stat, write_user->content);
            w.link(sys_time_stat->stat, write_sys->content);

            
            analysis_id++;
        }
        
        w.start();
        
    }

    
    
};

#endif /* subworkflows_hpp */
