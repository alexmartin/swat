#ifndef stabilitywarmupgraphs_hpp
#define stabilitywarmupgraphs_hpp

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>
#include <swat/core/workflow.hpp>

#include <swat/trace/gnuplot.hpp>

#include <swat/utils/constant.hpp>
#include <swat/utils/counter.hpp>

#include <set>

// computes stability graph and warmup graph based on given value streams

template <class T>
class StabilityWarmupGraphs: virtual public Module {
private:
    int size;
    
public:
    
    InputPort<T, Stream> *data_stream;
    InputPort<std::string, Value> *serie_name;
    
    InputPort<std::string, Value> graph_title;
    InputPort<std::string, Value> graph_path;
    
    StabilityWarmupGraphs(int size=1){
        this->size = size;
        data_stream = new InputPort<T, Stream> [size];
        serie_name = new InputPort<std::string, Value> [size];
    }
    
    void compute(){
        
        Workflow w;
                
        // Boxplots to display distribution
        PlotBoxplot<T> *boxplot = new PlotBoxplot<T> (size);
        w.addModule(*boxplot);
        
        // Lineplot to see warmup effect
        PlotLine<T> *lineplot = new PlotLine<T> (size);
        w.addModule(*lineplot);
        
        std::string title = graph_title.get();
        
        // set title
        Constant<std::string> title_boxplot (title + "-stability");
        w.addModule(title_boxplot);
        
        Constant<std::string> title_lineplot (title + "-warmup");
        w.addModule(title_lineplot);
        
        // link boxplot + lineplot names
        w.link(title_boxplot.constant_value_out, boxplot->graph_title);
        w.link(title_lineplot.constant_value_out, lineplot->graph_title);

        
        // set dest path
        w.link_inputs(graph_path, boxplot->graph_path);
        w.link_inputs(graph_path, lineplot->graph_path);

        for(int id=0; id<size; id++){
            
            // link data stream to plots modules
            w.link_inputs(data_stream[id], boxplot->data_stream[id]);
            w.link_inputs(data_stream[id], lineplot->data_stream[id]);
            
            // link names to plots modules
            w.link_inputs(serie_name[id], boxplot->data_title[id]);
            w.link_inputs(serie_name[id], lineplot->data_title[id]);
            
        }
        
        w.start();

    
    }

};

template<class T>
std::string get_str(T e){
    return std::to_string(e);
};

template<>
std::string get_str(std::string e){
    return e;
};

template<>
std::string get_str(state_t e){
    switch (e) {
        case 0:
            return std::string("unknow");
        case 1:
            return std::string("user");
        case 2:
            return std::string("kernel");
        case 3:
            return std::string("idle");
        case 4:
            return std::string("irq");
            
        default:
            return std::string("none");
    }
};

template <class K, class V>
class StabilityWarmupGraphs<Counter<K,V>>: virtual public Module {
    
public:
    
    InputPort<Counter<K,V>,Stream> counter_stream;
    
    InputPort<std::string, Value> graph_title;
    InputPort<std::string, Value> graph_path;
    
    
    void compute(){
        
        // for counter stream, use N values to stream (N = num of keys)
        //
        Workflow w;
        
        
        // agregate counters in a list
        // create VtS modules for Counter size
        // link counters to value to stream
        // link to "real" StabilityWarmupGraphs
        
        std::list<Counter<K,V>> counter_list;
        
        while (true){
            
            try {
                counter_list.push_back(counter_stream.recv());
            } catch (end_of_data) {
                break;
            }
            
        }
    
        // map to keep value to stream modules
        std::map<K, ValuesToStream<V>*> vts_map;
        
        std::set<K> key_set;
        
        //get list of all keys for all counters
        for(auto counter: counter_list){
            for(auto pair: counter.get_map()){
                key_set.insert(pair.first);
            }
        }
        
        // for each keys
       
        for(auto key: key_set){
            
            // create a VTS module
            ValuesToStream<V> *vts = new ValuesToStream<V> ((int)counter_list.size());
            vts_map[key] = vts;
            w.addModule(*vts);
            
            // for each counters
            int iter = 0;
            for(auto counter: counter_list){
                
                //get value for the key, if key not present return default value( zero)
                ConstantModule<V> *value = new  ConstantModule<V> (counter.get(key));
                w.addModule(*value);
                
                w.link(value->constant_value_out, vts->data[iter]);
                
                iter++;
            }
            
            
        }
    
        StabilityWarmupGraphs<V> *swg = new StabilityWarmupGraphs<V>((int)vts_map.size());
        w.addModule(*swg);
        
        // for all VTS modules, link to SWG module

        int iter=0;
        for(auto pair: vts_map){
            
            ConstantModule<std::string> *value = new ConstantModule<std::string> (get_str<K>(pair.first));
            
            w.addModule(*value);
            
            w.link(pair.second->datastream, swg->data_stream[iter]);
            w.link(value->constant_value_out, swg->serie_name[iter]);
            
            iter++;
        }
        
        
        w.link_inputs(graph_title, swg->graph_title);
        w.link_inputs(graph_path, swg->graph_path);
        
        
        w.start();
        
    }
    
};

#endif /* stabilitywarmupgraphs_hpp */
