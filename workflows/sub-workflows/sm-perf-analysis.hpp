#ifndef sm_perf_analysis_h
#define sm_perf_analysis_h

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>
#include <swat/core/workflow.hpp>

#include <swat/utils/stat.hpp>
#include <swat/utils/streamtovalues.hpp>
#include <swat/utils/valuestostream.hpp>
#include <swat/utils/constant.hpp>
#include <swat/utils/file.hpp>
#include <swat/utils/stringoperation.hpp>
#include <swat/utils/streamoperations.hpp>

#include <swat/trace/addition.hpp>
#include <swat/trace/state-filter.hpp>
#include <swat/trace/cpus-usage.hpp>
#include <swat/trace/states-duration.hpp>
#include <swat/trace/perf-module.hpp>
#include <swat/trace/type-event.hpp>
#include <swat/trace/gnuplot.hpp>
#include <swat/trace/ctf-reader.hpp>


#include "stabilitywarmupgraphs.hpp"


extern int number_of_runs;
extern std::string output_base;

class GetPerfValues: virtual public Module {
    
public:
    
    InputPort<std::string, Value> trace;
    InputPort<std::string, Value> bench_name;
    
    OutputPort<uint64_t, Value> mem_instr;
    OutputPort<uint64_t, Value> cpu_instr;
    
    void compute(){
        
        Workflow w;
        
        ConstantModule<std::string> *filename  = new ConstantModule<std::string> (trace.get());
        w.addModule(*filename);
        
        CTFReader *reader = new CTFReader;
        w.addModule(*reader);
        w.link(filename->constant_value_out, reader->filename);
        
        
        PerfNormalization *perf_norm = new PerfNormalization;
        w.addModule(*perf_norm);
        w.link(reader->events_stream, perf_norm->input_stream);
        
        ProcessAnalysis *process = new ProcessAnalysis;
        w.addModule(*process);
        w.link(perf_norm->output_stream, process->event_stream);
        
        std::string benchmark_name = bench_name.get();
        ProcessFilter<EventPunctual> *prodfilter_punctual = new ProcessFilter<EventPunctual> (benchmark_name);
        w.addModule(*prodfilter_punctual);

        w.link(process->event_punctual_stream, prodfilter_punctual->input_stream);
        
        PerfValues *perf = new PerfValues;
        w.addModule(*perf);
        w.link(prodfilter_punctual->matched_events, perf->input_stream);
        
        EventValueStreamAddition<uint64_t> *add_mem = new EventValueStreamAddition<uint64_t>;
        EventValueStreamAddition<uint64_t> *add_cpu = new EventValueStreamAddition<uint64_t>;
        w.addModule(*add_cpu);
        w.addModule(*add_mem);
        w.link(perf->mem_instr, add_mem->data_stream);
        w.link(perf->cpu_instr, add_cpu->data_stream);

        
        
        w.link_outputs(mem_instr, add_mem->addition_value);
        w.link_outputs(cpu_instr, add_cpu->addition_value);
        
        
        w.start();
        
    }
    
};

// compute memory data for all runs, print graph + detail stat file for values
class GetPerfAllRuns: virtual public Module {
    
public:
    
    InputPort<std::string, Value> bench_path;
    InputPort<std::string, Value> bench_name;
    InputPort<std::string, Value> res_path;
    
    void compute(){
        
        int num_runs = number_of_runs;
        
        std::string benchmark = bench_name.get();
        std::string benchmark_path = bench_path.get();
        //std::string result_path = res_path.get();
        std::string result_path = output_base + "/perf-analysis/";
        
        Workflow w;

        ValuesToStream<uint64_t> *vts_mem_instr = new ValuesToStream<uint64_t> (num_runs);
        ValuesToStream<uint64_t> *vts_cpu_instr = new ValuesToStream<uint64_t> (num_runs);
        w.addModule(*vts_mem_instr);
        w.addModule(*vts_cpu_instr);
        
        // mem
        StabilityWarmupGraphs<uint64_t> *instr_stability = new StabilityWarmupGraphs<uint64_t> (2);
        w.addModule(*instr_stability);
        Constant<std::string> *path_instr_graph = new Constant<std::string> (result_path);
        w.addModule(*path_instr_graph);
        w.link(path_instr_graph->constant_value_out, instr_stability->graph_path);
        w.link(vts_mem_instr->datastream, instr_stability->data_stream[0]);
        w.link(vts_cpu_instr->datastream, instr_stability->data_stream[1]);
        Constant<std::string> *event_serie_name_mem = new Constant<std::string> ("mem instructions");
        Constant<std::string> *event_serie_name_cpu = new Constant<std::string> ("cpu instructions");
        w.addModule(*event_serie_name_mem);
        w.addModule(*event_serie_name_cpu);
        w.link(event_serie_name_mem->constant_value_out, instr_stability->serie_name[0]);
        w.link(event_serie_name_cpu->constant_value_out, instr_stability->serie_name[1]);
        
        Constant<std::string> *graph_title = new Constant<std::string> (benchmark+"-instr");
        w.addModule(*graph_title);
        w.link(graph_title->constant_value_out, instr_stability->graph_title);
        
        //get values for all runs
        for(int i=0; i<num_runs; i++){
            // memory usage analysis
            GetPerfValues *gpv = new GetPerfValues;
            w.addModule(*gpv);
            
            Constant<std::string> *trace = new Constant<std::string> (benchmark_path+benchmark+"-perf-trace-run"+std::to_string(i)+"/");
            w.addModule(*trace);
            Constant<std::string> *bench = new Constant<std::string> (benchmark);
            w.addModule(*bench);
            
            w.link(trace->constant_value_out, gpv->trace);
            w.link(bench->constant_value_out, gpv->bench_name);
            
            w.link(gpv->mem_instr, vts_mem_instr->data[i]);
            w.link(gpv->cpu_instr, vts_cpu_instr->data[i]);
    
        }
        
        // mem
        ComputeStat<int64_t> *mem_instr_stats = new ComputeStat<int64_t>;
        w.addModule(*mem_instr_stats);
        w.link(vts_mem_instr->datastream, mem_instr_stats->data_in);
        WriteFile<Stat<int64_t>> *w_mem_instr_stat = new WriteFile<Stat<int64_t>>;
        w.addModule(*w_mem_instr_stat);
        Constant<std::string> *mem_instr_file = new Constant<std::string> (result_path+benchmark+"-memory-instructions.txt");
        w.addModule(*mem_instr_file);
        w.link(mem_instr_file->constant_value_out, w_mem_instr_stat->path);
        w.link(mem_instr_stats->stat, w_mem_instr_stat->content);
        
        // cpu
        ComputeStat<int64_t> *cpu_instr_stats = new ComputeStat<int64_t>;
        w.addModule(*cpu_instr_stats);
        w.link(vts_cpu_instr->datastream, cpu_instr_stats->data_in);
        WriteFile<Stat<int64_t>> *w_cpu_instr_stat = new WriteFile<Stat<int64_t>>;
        w.addModule(*w_cpu_instr_stat);
        Constant<std::string> *cpu_instr_file = new Constant<std::string> (result_path+benchmark+"-cpu-instructions.txt");
        w.addModule(*cpu_instr_file);
        w.link(cpu_instr_file->constant_value_out, w_cpu_instr_stat->path);
        w.link(cpu_instr_stats->stat, w_cpu_instr_stat->content);
        
        
        w.start();
        
        
    }
    
    
    
};


#endif /* sm_perf_analysis_h */
