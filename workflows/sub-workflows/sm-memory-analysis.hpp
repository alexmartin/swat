#ifndef sm_memory_analysis_h
#define sm_memory_analysis_h

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>
#include <swat/core/workflow.hpp>

#include <swat/utils/counter.hpp>
#include <swat/utils/streamtovalues.hpp>
#include <swat/utils/valuestostream.hpp>
#include <swat/utils/constant.hpp>
#include <swat/utils/file.hpp>
#include <swat/utils/stringoperation.hpp>
#include <swat/utils/stat.hpp>
#include <swat/utils/stdout.hpp>
#include <swat/utils/streamoperations.hpp>


#include <swat/trace/gnuplot.hpp>
#include <swat/trace/processes-analysis.hpp>
#include <swat/trace/memory-analysis.hpp>
#include <swat/trace/process-filter.hpp>
#include <swat/trace/ctf-reader.hpp>

#include "stabilitywarmupgraphs.hpp"

extern int number_of_runs;
extern std::string output_base;

// Sub workflow that output Value events
class GetMemoryValues: virtual public Module {

public:
    
    InputPort<std::string, Value> trace;
    InputPort<std::string, Value> bench_name;

    
    OutputPort<EventValue<int64_t>, Stream> event_value_stream;
    
    OutputPort<int64_t, Value> max_allocated_memory;
    OutputPort<int64_t, Value> useless_free_calls;
    OutputPort<int64_t, Value> realloc_calls_as_malloc;
    
    OutputPort<Counter<std::string, int64_t>, Value> function_calls;
    OutputPort<Counter<int64_t, int64_t>, Value> allocations_size;
    
    
    void compute(){
        
        /*
         
         open memory trace (memory trace = user trace + scheduling part)
         read events
         construct process analysis
         filter only events from the benchmark
         analysis memory
         output values
         
        */
       

        
        
        Workflow w;
        
        ConstantModule<std::string> *filename  = new ConstantModule<std::string> (trace.get());
        w.addModule(*filename);
        
        CTFReader *reader = new CTFReader(true);
        w.addModule(*reader);
        w.link(filename->constant_value_out, reader->filename);
        
        ProcessAnalysis *process = new ProcessAnalysis;
        w.addModule(*process);
        w.link(reader->events_stream, process->event_stream);
        
        std::string benchmark_name = bench_name.get();
        ProcessFilter<EventPunctual> *prodfilter_punctual = new ProcessFilter<EventPunctual> ("./" + benchmark_name);
        w.addModule(*prodfilter_punctual);
        w.link(process->event_punctual_stream, prodfilter_punctual->input_stream);

        MemoryAnalysis *ma = new MemoryAnalysis;
        w.addModule(*ma);
        
        w.link(prodfilter_punctual->matched_events, ma->event_stream);
        //w.link(process->event_punctual_stream, ma->event_stream);
        
        w.link_outputs(event_value_stream, ma->values_stream);
        w.link_outputs(max_allocated_memory, ma->max_allocated_memory);
        w.link_outputs(useless_free_calls, ma->useless_free_calls);
        w.link_outputs(realloc_calls_as_malloc, ma->realloc_calls_as_malloc);
        w.link_outputs(function_calls, ma->function_calls);
        w.link_outputs(allocations_size, ma->allocations_size);
        
        w.start();
        
    }
    
};

// compute memory data for all runs, print graph + detail stat file for values
class GetMemoryAllRuns: virtual public Module {
    
public:
    
    InputPort<std::string, Value> bench_path;
    InputPort<std::string, Value> bench_name;
    InputPort<std::string, Value> res_path;
    
    void compute(){
        
        std::string benchmark = bench_name.get();
        std::string benchmark_path = bench_path.get();
        //std::string result_path = res_path.get();
        std::string result_path = output_base + "/memory-analysis/";
        
        Workflow w;
        
        int num_runs = number_of_runs;
        
        PlotLine<EventValue<int64_t>> *plot = new PlotLine<EventValue<int64_t>> (num_runs);
        w.addModule(*plot);
        
        ValuesToStream<int64_t> *vts_max_memory = new ValuesToStream<int64_t> (num_runs);
        ValuesToStream<int64_t> *vts_useless_free = new ValuesToStream<int64_t> (num_runs);
        ValuesToStream<int64_t> *vts_realloc_as_malloc = new ValuesToStream<int64_t> (num_runs);
        ValuesToStream<Counter<std::string, int64_t>> *vts_function_calls = new ValuesToStream<Counter<std::string, int64_t>> (num_runs);
        ValuesToStream<Counter<int64_t, int64_t>> *vts_allocations_size = new ValuesToStream<Counter<int64_t, int64_t>> (num_runs);
        w.addModule(*vts_max_memory);
        w.addModule(*vts_useless_free);
        w.addModule(*vts_realloc_as_malloc);
        w.addModule(*vts_function_calls);
        w.addModule(*vts_allocations_size);
        
        // max_memory
        StabilityWarmupGraphs<int64_t> *max_memory_stability = new StabilityWarmupGraphs<int64_t>;
        w.addModule(*max_memory_stability);
        Constant<std::string> *path_max_memory_graph = new Constant<std::string> (result_path);
        w.addModule(*path_max_memory_graph);
        w.link(path_max_memory_graph->constant_value_out, max_memory_stability->graph_path);
        w.link(vts_max_memory->datastream, max_memory_stability->data_stream[0]);
        Constant<std::string> *max_memory_name = new Constant<std::string> ("maximum memory");
        w.addModule(*max_memory_name);
        w.link(max_memory_name->constant_value_out, max_memory_stability->serie_name[0]);

        // useless_free
        StabilityWarmupGraphs<int64_t> *useless_free_stability = new StabilityWarmupGraphs<int64_t>;
        w.addModule(*useless_free_stability);
        Constant<std::string> *path_useless_free_graph = new Constant<std::string> (result_path);
        w.addModule(*path_useless_free_graph);
        w.link(path_useless_free_graph->constant_value_out, useless_free_stability->graph_path);
        w.link(vts_useless_free->datastream, useless_free_stability->data_stream[0]);
        Constant<std::string> *useless_free_name = new Constant<std::string> ("useless free");
        w.addModule(*useless_free_name);
        w.link(useless_free_name->constant_value_out, useless_free_stability->serie_name[0]);
        
        // realloc_as_malloc
        StabilityWarmupGraphs<int64_t> *realloc_as_malloc_stability = new StabilityWarmupGraphs<int64_t>;
        w.addModule(*realloc_as_malloc_stability);
        Constant<std::string> *path_realloc_as_malloc_graph = new Constant<std::string> (result_path);
        w.addModule(*path_realloc_as_malloc_graph);
        w.link(path_realloc_as_malloc_graph->constant_value_out, realloc_as_malloc_stability->graph_path);
        w.link(vts_realloc_as_malloc->datastream, realloc_as_malloc_stability->data_stream[0]);
        Constant<std::string> *realloc_as_malloc_name = new Constant<std::string> ("realloc as malloc");
        w.addModule(*realloc_as_malloc_name);
        w.link(realloc_as_malloc_name->constant_value_out, realloc_as_malloc_stability->serie_name[0]);
        
        // function_calls
//        StabilityWarmupGraphs<Counter<std::string, int64_t>> *function_calls_stability = new StabilityWarmupGraphs<Counter<std::string, int64_t>>;
//        w.addModule(*function_calls_stability);
//        Constant<std::string> *path_function_calls_graph = new Constant<std::string> (result_path);
//        w.addModule(*path_function_calls_graph);
//        w.link(path_function_calls_graph->constant_value_out, function_calls_stability->graph_path);
//        w.link(vts_function_calls->datastream, function_calls_stability->counter_stream);
        
        // allocations_size
//        StabilityWarmupGraphs<Counter<uint64_t, uint64_t>> *allocations_size_stability = new StabilityWarmupGraphs<Counter<uint64_t, uint64_t>>;
//        w.addModule(*allocations_size_stability);
//        Constant<std::string> *path_allocations_size_graph = new Constant<std::string> (result_path);
//        w.addModule(*path_allocations_size_graph);
//        w.link(path_allocations_size_graph->constant_value_out, allocations_size_stability->graph_path);
//        w.link(vts_allocations_size->datastream, allocations_size_stability->counter_stream);
        
        // get values for the runs
        for(int i=0; i<num_runs; i++){
            // memory usage analysis
            GetMemoryValues *gmv = new GetMemoryValues;
            w.addModule(*gmv);
            Constant<std::string> *trace = new Constant<std::string> (benchmark_path+benchmark+"-libc-run"+std::to_string(i)+"/");
            w.addModule(*trace);
            Constant<std::string> *bench = new Constant<std::string> (benchmark);
            w.addModule(*bench);
            w.link(trace->constant_value_out, gmv->trace);
            w.link(bench->constant_value_out, gmv->bench_name);

            w.link(gmv->event_value_stream,plot->data_stream[i]);
            Constant<std::string> *title = new Constant<std::string> ("run"+std::to_string(i));
            w.addModule(*title);

            w.link(title->constant_value_out, plot->data_title[i]);
            
            w.link(gmv->max_allocated_memory, vts_max_memory->data[i]);
            w.link(gmv->useless_free_calls, vts_useless_free->data[i]);
            w.link(gmv->realloc_calls_as_malloc, vts_realloc_as_malloc->data[i]);
            w.link(gmv->function_calls, vts_function_calls->data[i]);
            w.link(gmv->allocations_size, vts_allocations_size->data[i]);

            
        }
        
        Constant<std::string> *graph_title = new Constant<std::string> (benchmark+"-memory");
        Constant<std::string> *graph_path = new Constant<std::string> (result_path);
        w.addModule(*graph_title);
        w.addModule(*graph_path);
        
        w.link(graph_title->constant_value_out, plot->graph_title);
        w.link(graph_path->constant_value_out, plot->graph_path);
        
        
        // max_memory
        Constant<std::string> *graph_title_max_memory = new Constant<std::string> (benchmark+"-max-memory");
        w.addModule(*graph_title_max_memory);
        w.link(graph_title_max_memory->constant_value_out, max_memory_stability->graph_title);
        ComputeStat<int64_t> *max_memory_stats = new ComputeStat<int64_t>;
        w.addModule(*max_memory_stats);
        w.link(vts_max_memory->datastream, max_memory_stats->data_in);
        WriteFile<Stat<int64_t>> *w_max_memory_stat = new WriteFile<Stat<int64_t>>;
        w.addModule(*w_max_memory_stat);
        Constant<std::string> *max_memory_file = new Constant<std::string> (result_path+benchmark+"-max-memory.txt");
        w.addModule(*max_memory_file);
        w.link(max_memory_file->constant_value_out, w_max_memory_stat->path);
        w.link(max_memory_stats->stat, w_max_memory_stat->content);

        // useless_free
        Constant<std::string> *graph_title_useless_free = new Constant<std::string> (benchmark+"-useless-free");
        w.addModule(*graph_title_useless_free);
        w.link(graph_title_useless_free->constant_value_out, useless_free_stability->graph_title);
        ComputeStat<int64_t> *useless_free_stats = new ComputeStat<int64_t>;
        w.addModule(*useless_free_stats);
        w.link(vts_useless_free->datastream, useless_free_stats->data_in);
        WriteFile<Stat<int64_t>> *w_useless_free_stat = new WriteFile<Stat<int64_t>>;
        w.addModule(*w_useless_free_stat);
        Constant<std::string> *useless_free_file = new Constant<std::string> (result_path+benchmark+"-useless-free.txt");
        w.addModule(*useless_free_file);
        w.link(useless_free_file->constant_value_out, w_useless_free_stat->path);
        w.link(useless_free_stats->stat, w_useless_free_stat->content);
        
        // realloc_as_malloc
        Constant<std::string> *graph_title_realloc_as_malloc = new Constant<std::string> (benchmark+"-realloc-as-malloc");
        w.addModule(*graph_title_realloc_as_malloc);
        w.link(graph_title_realloc_as_malloc->constant_value_out, realloc_as_malloc_stability->graph_title);
        ComputeStat<int64_t> *realloc_as_malloc_stats = new ComputeStat<int64_t>;
        w.addModule(*realloc_as_malloc_stats);
        w.link(vts_realloc_as_malloc->datastream, realloc_as_malloc_stats->data_in);
        WriteFile<Stat<int64_t>> *w_realloc_as_malloc_stat = new WriteFile<Stat<int64_t>>;
        w.addModule(*w_realloc_as_malloc_stat);
        Constant<std::string> *realloc_as_malloc_file = new Constant<std::string> (result_path+benchmark+"-realloc-as-malloc.txt");
        w.addModule(*realloc_as_malloc_file);
        w.link(realloc_as_malloc_file->constant_value_out, w_realloc_as_malloc_stat->path);
        w.link(realloc_as_malloc_stats->stat, w_realloc_as_malloc_stat->content);
        
        // function_calls
//        Constant<std::string> *graph_title_function_calls = new Constant<std::string> (benchmark+"-function-calls");
//        w.addModule(*graph_title_function_calls);
//        w.link(graph_title_function_calls->constant_value_out, function_calls_stability->graph_title);
//        ComputeStat<Counter<std::string, int64_t>> *function_calls_stats = new ComputeStat<Counter<std::string, int64_t>>;
//        w.addModule(*function_calls_stats);
//        w.link(vts_function_calls->datastream, function_calls_stats->data_in);
//        WriteFile<Stat<Counter<std::string, int64_t>>> *w_function_calls_stat = new WriteFile<Stat<Counter<std::string, int64_t>>>;
//        w.addModule(*w_function_calls_stat);
//        Constant<std::string> *function_calls_file = new Constant<std::string> (result_path+benchmark+"-function-calls.txt");
//        w.addModule(*function_calls_file);
//        w.link(function_calls_file->constant_value_out, w_function_calls_stat->path);
//        w.link(function_calls_stats->stat, w_function_calls_stat->content);
        
        // allocations_size
//        Constant<std::string> *graph_title_allocations_size = new Constant<std::string> (benchmark+"-allocations-size");
//        w.addModule(*graph_title_allocations_size);
//        w.link(graph_title_allocations_size->constant_value_out, allocations_size_stability->graph_title);
//        ComputeStat<Counter<std::string, int64_t>> *allocations_size_stats = new ComputeStat<Counter<std::string, int64_t>>;
//        w.addModule(*allocations_size_stats);
//        w.link(vts_allocations_size->datastream, allocations_size_stats->data_in);
//        WriteFile<Stat<Counter<std::string, int64_t>>> *w_allocations_size_stat = new WriteFile<Stat<Counter<std::string, int64_t>>>;
//        w.addModule(*w_allocations_size_stat);
//        Constant<std::string> *allocations_size_file = new Constant<std::string> (result_path+benchmark+"-allocations-size.txt");
//        w.addModule(*allocations_size_file);
//        w.link(allocations_size_file->constant_value_out, w_allocations_size_stat->path);
//        w.link(allocations_size_stats->stat, w_allocations_size_stat->content);
        
        
        w.start();
        
        
    }
    
    
    
};

#endif /* sm_memory_analysis_h */
