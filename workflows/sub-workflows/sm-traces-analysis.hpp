#ifndef sm_traces_analysis_h
#define sm_traces_analysis_h

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>
#include <swat/core/workflow.hpp>

#include <swat/utils/streamtovalues.hpp>
#include <swat/utils/valuestostream.hpp>
#include <swat/utils/constant.hpp>
#include <swat/utils/file.hpp>
#include <swat/utils/stringoperation.hpp>
#include <swat/utils/stat.hpp>
#include <swat/utils/streamoperations.hpp>

#include <swat/trace/gnuplot.hpp>

#include "stabilitywarmupgraphs.hpp"

extern std::string output_base;

class GetTraceValue: virtual public Module {
    
public:
    
    InputPort<std::string, Value> path;
    
    OutputPort<Stat<uint64_t>, Value> stat;
    
    void compute(){
        
        
        Workflow w;
        
        // open and read file
        ReadFile<std::string> *rf = new ReadFile<std::string>;
        w.addModule(*rf);
        
        w.link_inputs(path, rf->path);

        StringToUint64 *s = new StringToUint64;
        w.addModule(*s);
        
        w.link(rf->content, s->string_stream);
        
        ComputeStat<uint64_t> *stats = new ComputeStat<uint64_t>;
        w.addModule(*stats);

        w.link(s->ull_stream, stats->data_in);
        
        w.link_outputs(stat, stats->stat);
        
        w.start();
        
    }
    
};


// all analaysis of time files for one bench
class GetTraceAllAnalysis: virtual public Module {
    
public:
    
    InputPort<std::string, Value> bench_path;
    InputPort<std::string, Value> bench_name;
    InputPort<std::string, Value> res_path;
    
    void compute(){
        
        std::string benchmark = bench_name.get();
        std::string benchmark_path = bench_path.get();
        //std::string result_path = res_path.get();
        std::string result_path = output_base + "/trace-analysis/";
        
        Workflow w;
        
        // size
        std::list<std::string> size_list = {"all-events","libc","perf-trace"};

        for(auto s :size_list){
            
            Constant<std::string> *path = new Constant<std::string> (benchmark_path+"res-size/"+benchmark+"-"+s+"-size.txt");
            w.addModule(*path);
            
            Constant<std::string> *res_file  = new Constant<std::string> (result_path+benchmark+"-"+s+"-size.txt");
            w.addModule(*res_file);
            
            GetTraceValue *gtv = new GetTraceValue;
            w.addModule(*gtv);
            
            w.link(path->constant_value_out, gtv->path);
            
            WriteFile<Stat<uint64_t>> *write = new WriteFile<Stat<uint64_t>>;
            w.addModule(*write);

            w.link(res_file->constant_value_out, write->path);
            w.link(gtv->stat, write->content);

            
        }
        
        std::list<std::string> num_events_list = {"all-events","libc","libc-u","perf-trace"};
        
        for(auto s :num_events_list){
            
            Constant<std::string> *path = new Constant<std::string> (benchmark_path+"res-num-events/"+benchmark+"-"+s+".txt");
            w.addModule(*path);
            
            Constant<std::string> *res_file  = new Constant<std::string> (result_path+benchmark+"-"+s+".txt");
            w.addModule(*res_file);
            
            GetTraceValue *gtv = new GetTraceValue;
            w.addModule(*gtv);
            
            w.link(path->constant_value_out, gtv->path);
            
            WriteFile<Stat<uint64_t>> *write = new WriteFile<Stat<uint64_t>>;
            w.addModule(*write);
            
            w.link(res_file->constant_value_out, write->path);
            w.link(gtv->stat, write->content);
            
        }

        
        w.start();
        
        
    }
    
    
    
};


#endif /* sm_traces_analysis_h */
