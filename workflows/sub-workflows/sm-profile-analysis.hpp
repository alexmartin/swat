#ifndef sm_profile_analysis_h
#define sm_profile_analysis_h

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>
#include <swat/core/workflow.hpp>

#include <swat/utils/streamtovalues.hpp>
#include <swat/utils/valuestostream.hpp>
#include <swat/utils/constant.hpp>
#include <swat/utils/file.hpp>
#include <swat/utils/stringoperation.hpp>
#include <swat/utils/stat.hpp>
#include <swat/utils/streamoperations.hpp>

#include <swat/trace/countermodule.hpp>
#include <swat/trace/state-filter.hpp>
#include <swat/trace/cpus-usage.hpp>
#include <swat/trace/states-duration.hpp>
#include <swat/trace/compute-duration.hpp>
#include <swat/trace/module-category.hpp>
#include <swat/trace/type-event.hpp>
#include <swat/trace/gnuplot.hpp>
#include <swat/trace/ctf-reader.hpp>

#include "stabilitywarmupgraphs.hpp"

extern int number_of_runs;
extern std::string output_base;

class GetProfileValues: virtual public Module {
    
public:
    
    InputPort<std::string, Value> trace;
    InputPort<std::string, Value> bench_name;
    
    OutputPort<Counter<std::string, uint64_t>, Value> category_counter;
    OutputPort<Counter<int64_t, uint64_t>, Value> cpu_counter;
    OutputPort<Counter<state_t, uint64_t>, Value> states_counter;
    OutputPort<uint64_t, Value> event_number;
    OutputPort<uint64_t, Value> duration;
    
    void compute(){
        
        Workflow w;
        
        ConstantModule<std::string> *filename  = new ConstantModule<std::string> (trace.get());
        w.addModule(*filename);
        
        CTFReader *reader = new CTFReader;
        w.addModule(*reader);
        
        w.link(filename->constant_value_out, reader->filename);
        
        ProcessAnalysis *process = new ProcessAnalysis;
        w.addModule(*process);
        
        w.link(reader->events_stream, process->event_stream);
        
        std::string benchmark_name = bench_name.get();
        ProcessFilter<EventState> *prodfilter_state = new ProcessFilter<EventState> ("./" + benchmark_name);
        ProcessFilter<EventPunctual> *prodfilter_punctual = new ProcessFilter<EventPunctual> ("./" + benchmark_name);
        w.addModule(*prodfilter_state);
        w.addModule(*prodfilter_punctual);
        
        w.link(process->event_state_stream, prodfilter_state->input_stream);
        w.link(process->event_punctual_stream, prodfilter_punctual->input_stream);
        
        Category *cat = new Category;
        w.addModule(*cat);
        w.link(prodfilter_punctual->matched_events, cat->event_stream);
        w.link_outputs(category_counter, cat->category_counter);
        
        CounterModule<EventPunctual> *counter = new CounterModule<EventPunctual>;
        w.addModule(*counter);
        w.link(prodfilter_punctual->matched_events, counter->data_stream);
        w.link_outputs(event_number, counter->number);
        //w.link_outputs(event_number, reader->events_number);
        
        StateFilter *filter = new StateFilter (IDLE);
        CPUsUsage *cpu_durations = new CPUsUsage;
        w.addModule(*filter);
        w.addModule(*cpu_durations);
        w.link(prodfilter_state->matched_events, filter->input_stream);
        w.link(filter->discarded_events, cpu_durations->stream_event);
        w.link_outputs(cpu_counter, cpu_durations->cpus_counters);
        
        
        StateDuration *state_dur = new StateDuration;
        w.addModule(*state_dur);
        w.link(prodfilter_state->matched_events, state_dur->states_stream);
        w.link_outputs(states_counter, state_dur->states_duration);


        ComputeDuration *comp_dur = new ComputeDuration;
        w.addModule(*comp_dur);
        w.link(prodfilter_state->matched_events, comp_dur->states_stream);
        w.link_outputs(duration, comp_dur->duration);
        
        
        w.start();
        
    }
    
};

// compute memory data for all runs, print graph + detail stat file for values
class GetProfileAllRuns: virtual public Module {
    
public:
    
    InputPort<std::string, Value> bench_path;
    InputPort<std::string, Value> bench_name;
    InputPort<std::string, Value> res_path;
    
    void compute(){
        
        std::string benchmark = bench_name.get();
        std::string benchmark_path = bench_path.get();
        //std::string result_path = res_path.get();
        std::string result_path = output_base + "/profile-analysis/";
        
        Workflow w;
        
        int num_runs = number_of_runs;
        

        ValuesToStream<Counter<std::string, uint64_t>> *vts_category_counter = new ValuesToStream<Counter<std::string, uint64_t>> (num_runs);
        ValuesToStream<Counter<uint64_t, uint64_t>> *vts_cpu_counter = new ValuesToStream<Counter<uint64_t, uint64_t>> (num_runs);
        ValuesToStream<Counter<state_t, uint64_t>> *vts_states_counter = new ValuesToStream<Counter<state_t, uint64_t>> (num_runs);
        ValuesToStream<uint64_t> *vts_event_number = new ValuesToStream<uint64_t> (num_runs);
        ValuesToStream<uint64_t> *vts_duration = new ValuesToStream<uint64_t> (num_runs);
        w.addModule(*vts_category_counter);
        w.addModule(*vts_cpu_counter);
        w.addModule(*vts_states_counter);
        w.addModule(*vts_event_number);
        w.addModule(*vts_duration);
        
        // category
        StabilityWarmupGraphs<Counter<std::string, uint64_t>> *category_stability = new StabilityWarmupGraphs<Counter<std::string, uint64_t>>;
        w.addModule(*category_stability);
        Constant<std::string> *path_category_graph = new Constant<std::string> (result_path);
        w.addModule(*path_category_graph);
        w.link(path_category_graph->constant_value_out, category_stability->graph_path);
        w.link(vts_category_counter->datastream, category_stability->counter_stream);

        // cpu
        StabilityWarmupGraphs<Counter<uint64_t, uint64_t>> *cpu_stability = new StabilityWarmupGraphs<Counter<uint64_t, uint64_t>>;
        w.addModule(*cpu_stability);
        Constant<std::string> *path_cpu_graph = new Constant<std::string> (result_path);
        w.addModule(*path_cpu_graph);
        w.link(path_cpu_graph->constant_value_out, cpu_stability->graph_path);
        w.link(vts_cpu_counter->datastream, cpu_stability->counter_stream);

        // states
        StabilityWarmupGraphs<Counter<state_t, uint64_t>> *states_stability = new StabilityWarmupGraphs<Counter<state_t, uint64_t>>;
        w.addModule(*states_stability);
        Constant<std::string> *path_states_graph = new Constant<std::string> (result_path);
        w.addModule(*path_states_graph);
        w.link(path_states_graph->constant_value_out, states_stability->graph_path);
        w.link(vts_states_counter->datastream, states_stability->counter_stream);
        
        // Events number
        StabilityWarmupGraphs<uint64_t> *num_events_stability = new StabilityWarmupGraphs<uint64_t>;
        w.addModule(*num_events_stability);
        Constant<std::string> *path_event_graph = new Constant<std::string> (result_path);
        w.addModule(*path_event_graph);
        w.link(path_event_graph->constant_value_out, num_events_stability->graph_path);
        w.link(vts_event_number->datastream, num_events_stability->data_stream[0]);
        Constant<std::string> *event_serie_name = new Constant<std::string> ("number of events");
        w.addModule(*event_serie_name);
        w.link(event_serie_name->constant_value_out, num_events_stability->serie_name[0]);

        // duration
        StabilityWarmupGraphs<uint64_t> *duration_stability = new StabilityWarmupGraphs<uint64_t>;
        w.addModule(*duration_stability);
        Constant<std::string> *path_duration_graph = new Constant<std::string> (result_path);
        w.addModule(*path_duration_graph);
        w.link(path_duration_graph->constant_value_out, duration_stability->graph_path);
        w.link(vts_duration->datastream, duration_stability->data_stream[0]);
        Constant<std::string> *duration_serie_name = new Constant<std::string> ("trace duration");
        w.addModule(*duration_serie_name);
        w.link(duration_serie_name->constant_value_out, duration_stability->serie_name[0]);

        
        // get values for the 32 runs
        for(int i=0; i<num_runs; i++){
            // memory usage analysis
            GetProfileValues *gpv = new GetProfileValues;
            w.addModule(*gpv);
            
            Constant<std::string> *trace = new Constant<std::string> (benchmark_path+benchmark+"-all-events-run"+std::to_string(i)+"/");
            w.addModule(*trace);
            Constant<std::string> *bench = new Constant<std::string> (benchmark);
            w.addModule(*bench);
            
            w.link(trace->constant_value_out, gpv->trace);
            w.link(bench->constant_value_out, gpv->bench_name);
            
            w.link(gpv->category_counter, vts_category_counter->data[i]);
            w.link(gpv->cpu_counter, vts_cpu_counter->data[i]);
            w.link(gpv->states_counter, vts_states_counter->data[i]);
            w.link(gpv->event_number, vts_event_number->data[i]);
            w.link(gpv->duration, vts_duration->data[i]);
            
        }
        
        // category
        Constant<std::string> *graph_title_category = new Constant<std::string> (benchmark+"-category");
        w.addModule(*graph_title_category);
        w.link(graph_title_category->constant_value_out, category_stability->graph_title);
        ComputeStat<Counter<std::string, uint64_t>> *category_stats = new ComputeStat<Counter<std::string, uint64_t>>;
        w.addModule(*category_stats);
        w.link(vts_category_counter->datastream, category_stats->data_in);
        WriteFile<Stat<Counter<std::string, uint64_t>>> *w_category_stat = new WriteFile<Stat<Counter<std::string, uint64_t>>>;
        w.addModule(*w_category_stat);
        Constant<std::string> *category_file = new Constant<std::string> (result_path+benchmark+"-category.txt");
        w.addModule(*category_file);
        w.link(category_file->constant_value_out, w_category_stat->path);
        w.link(category_stats->stat, w_category_stat->content);
        
        // cpu
        Constant<std::string> *graph_title_cpu = new Constant<std::string> (benchmark+"-cpu");
        w.addModule(*graph_title_cpu);
        w.link(graph_title_cpu->constant_value_out, cpu_stability->graph_title);
        ComputeStat<Counter<uint64_t, uint64_t>> *cpu_stats = new ComputeStat<Counter<uint64_t, uint64_t>>;
        w.addModule(*cpu_stats);
        w.link(vts_cpu_counter->datastream, cpu_stats->data_in);
        WriteFile<Stat<Counter<uint64_t, uint64_t>>> *w_cpu_stat = new WriteFile<Stat<Counter<uint64_t, uint64_t>>>;
        w.addModule(*w_cpu_stat);
        Constant<std::string> *cpu_file = new Constant<std::string> (result_path+benchmark+"-cpu.txt");
        w.addModule(*cpu_file);
        w.link(cpu_file->constant_value_out, w_cpu_stat->path);
        w.link(cpu_stats->stat, w_cpu_stat->content);
        
        // states
        Constant<std::string> *graph_title_states = new Constant<std::string> (benchmark+"-states");
        w.addModule(*graph_title_states);
        w.link(graph_title_states->constant_value_out, states_stability->graph_title);
        ComputeStat<Counter<state_t, uint64_t>> *states_stats = new ComputeStat<Counter<state_t, uint64_t>>;
        w.addModule(*states_stats);
        w.link(vts_states_counter->datastream, states_stats->data_in);
        WriteFile<Stat<Counter<state_t, uint64_t>>> *w_states_stat = new WriteFile<Stat<Counter<state_t, uint64_t>>>;
        w.addModule(*w_states_stat);
        Constant<std::string> *states_file = new Constant<std::string> (result_path+benchmark+"-states.txt");
        w.addModule(*states_file);
        w.link(states_file->constant_value_out, w_states_stat->path);
        w.link(states_stats->stat, w_states_stat->content);
        
        // event number
        Constant<std::string> *graph_title_event = new Constant<std::string> (benchmark+"-events-number");
        w.addModule(*graph_title_event);
        w.link(graph_title_event->constant_value_out, num_events_stability->graph_title);
        ComputeStat<int64_t> *event_number_stats = new ComputeStat<int64_t>;
        w.addModule(*event_number_stats);
        w.link(vts_event_number->datastream, event_number_stats->data_in);
        WriteFile<Stat<int64_t>> *w_event_number_stat = new WriteFile<Stat<int64_t>>;
        w.addModule(*w_event_number_stat);
        Constant<std::string> *event_number_file = new Constant<std::string> (result_path+benchmark+"-events-number.txt");
        w.addModule(*event_number_file);
        w.link(event_number_file->constant_value_out, w_event_number_stat->path);
        w.link(event_number_stats->stat, w_event_number_stat->content);

        // duration
        Constant<std::string> *graph_title_duration = new Constant<std::string> (benchmark+"-duration");
        w.addModule(*graph_title_duration);
        w.link(graph_title_duration->constant_value_out, duration_stability->graph_title);
        ComputeStat<int64_t> *duration_stats = new ComputeStat<int64_t>;
        w.addModule(*duration_stats);
        w.link(vts_duration->datastream, duration_stats->data_in);
        WriteFile<Stat<int64_t>> *w_duration_stat = new WriteFile<Stat<int64_t>>;
        w.addModule(*w_duration_stat);
        Constant<std::string> *duration_file = new Constant<std::string> (result_path+benchmark+"-duration.txt");
        w.addModule(*duration_file);
        w.link(duration_file->constant_value_out, w_duration_stat->path);
        w.link(duration_stats->stat, w_duration_stat->content);

        
        w.start();
        
        
    }
    
    
    
};


#endif /* sm_profile_analysis_h */
