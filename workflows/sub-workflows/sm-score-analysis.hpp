#ifndef score_analysis_h
#define score_analysis_h

#include <swat/core/port.hpp>
#include <swat/core/module.hpp>
#include <swat/core/workflow.hpp>

#include <swat/trace/gnuplot.hpp>

#include <swat/utils/streamtovalues.hpp>
#include <swat/utils/valuestostream.hpp>
#include <swat/utils/constant.hpp>
#include <swat/utils/file.hpp>
#include <swat/utils/stringoperation.hpp>
#include <swat/utils/stat.hpp>
#include <swat/utils/streamoperations.hpp>

#include "stabilitywarmupgraphs.hpp"

extern std::string output_base;

// Sub workflow that compute analysis time file for one analysis
class GetScoreValues: virtual public Module {
    
public:
    
    InputPort<std::string, Value> ip_bench_base;
    
    OutputPort<float, Stream> score_stream;
    
    void compute(){
        
        std::string benchmark = ip_bench_base.get();
        
        Workflow w(benchmark);
    
        std::string filename = benchmark+".csv";
        
        // file name of result
        Constant<std::string> *file = new Constant<std::string>(filename);
        w.addModule(*file);
        
        // open and read file
        ReadFile<std::string> *rf = new ReadFile<std::string>;
        w.addModule(*rf);
        
        
        w.link(file->constant_value_out, rf->path);
            
        // split input stream into 1 line (file should contain 1 line only)
        StreamToValues<std::string> *stv = new StreamToValues<std::string> (1);
        w.addModule(*stv);
        
        w.link(rf->content, stv->datastream);
        
        // split line to values
        StringSplitter *ss = new StringSplitter;
        w.addModule(*ss);
        
        Constant<char> *delimiter = new Constant<char> (',');
        w.addModule(*delimiter);
        

        w.link(stv->datavalue[0],  ss->i_string_stream);
        w.link(delimiter->constant_value_out, ss->splitter);
        
        // skip first element of the stream ( descr of bench)
        SkipElements<std::string> *se = new SkipElements<std::string>;
        w.addModule(*se);
        
        Constant<int> *nb_skip = new Constant<int> (1);
        w.addModule(*nb_skip);
        
        w.link(nb_skip->constant_value_out, se->n);
        w.link(ss->o_string_stream ,se->datastream_in);
        
        
        // convert string to float
        StringToFloat *stf = new StringToFloat;
        w.addModule(*stf);

        w.link(se->datastream_out, stf->string_stream);

        w.link_outputs(score_stream, stf->float_stream);
        
        w.start();
        
    }
    
};

// all analaysis of time files for one bench
class GetScoreAllAnalysis: virtual public Module {
    
public:
    
    InputPort<std::string, Value> bench_path;
    InputPort<std::string, Value> bench_name;
    InputPort<std::string, Value> res_path;
    
    void compute(){
        
        std::string benchmark = bench_name.get();
        std::string benchmark_path = bench_path.get();
        //std::string result_path = res_path.get();
        std::string result_path = output_base + "/score-analysis/";
        
        Workflow w;
        
        std::list<std::string> analysis_list = {"all-events","libc","only","perf-trace","time"};
        
        // stability modules (for the 7 analysis)
        StabilityWarmupGraphs<float> *stability = new StabilityWarmupGraphs<float> (5);
        w.addModule(*stability);
        
        // set title
        Constant<std::string> title (benchmark+"-score");
        w.addModule(title);
        
        w.link(title.constant_value_out,stability->graph_title);
        
        
        // set dest path
        Constant<std::string> dest_path (result_path);
        
        w.addModule(dest_path);
        w.link(dest_path.constant_value_out,stability->graph_path);
        
        int analysis_id = 0;
        for(std::string analysis: analysis_list){
            
            Constant<std::string> *bench_base = new Constant<std::string>(benchmark_path+benchmark+"-"+analysis);
            w.addModule(*bench_base);
            
            Constant<std::string> *analysis_name = new Constant<std::string>(analysis);
            w.addModule(*analysis_name);
            
            GetScoreValues *gs = new GetScoreValues;
            w.addModule(*gs);
            
            w.link(bench_base->constant_value_out, gs->ip_bench_base);
            
            w.link(gs->score_stream, stability->data_stream[analysis_id]);
            
            w.link(analysis_name->constant_value_out, stability->serie_name[analysis_id]);
            
            // compute stats
            ComputeStat<float> *score_stat = new ComputeStat<float>;
            w.addModule(*score_stat);
            
            w.link(gs->score_stream, score_stat->data_in);

            
            // save stat to file
            Constant<std::string> *path = new Constant<std::string> (result_path+benchmark+"-"+analysis+"-score.txt");
            w.addModule(*path);
            
            WriteFile<Stat<float>> *write = new WriteFile<Stat<float>>;
            w.addModule(*write);
            
            w.link(path->constant_value_out, write->path);
            
            w.link(score_stat->stat, write->content);
                       
            
            analysis_id++;
        }
        
        w.start();
        
    }
    
    
    
};

#endif /* score_analysis_h */
