/* 

    Example workflow for trace reading
    It uses the ctfreader to construct events and then print them on the stdout

    compile with: g++ print-trace.cpp -lswatcore -lswattrace -lpthread -lbabeltrace-ctf

*/


#include <swat/core/workflow.hpp>

#include <swat/utils/constant.hpp>
#include <swat/utils/stdout.hpp>

#include <swat/trace/ctf-reader.hpp>
#include <swat/trace/type-event.hpp>

#include <string>

int main(int argc, const char * argv[]) {


    // check if argument was given
    if(argc!=2){
        std::cout << "You must give one argument : trace to read" << std::endl;
        return 1;
    }


    // create a new workflow
    Workflow w;

    // add constant module for path
    Constant<std::string> *filename  = new Constant<std::string> (argv[1]);
    w.addModule(*filename);

    // add reader for the CTF trace
    CTFReader *reader = new CTFReader;
    w.addModule(*reader);

    // link filename constant to the filename port of the reader module
    w.link(filename->constant_value_out, reader->filename);

    // add a stdout module to print trace
    Stdout<EventPunctual,Stream> *out = new Stdout<EventPunctual,Stream>;
    w.addModule(*out);

    // link events stream to stdout input data port
    w.link(reader->events_stream, out->data_stream);

    // run the workflow
    w.start();

    return 0;

}

