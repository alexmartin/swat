#include <swat/core/workflow.hpp>

// include sub-workflow definition
#include "sub-workflows/sm-time-analysis.hpp"
#include "sub-workflows/sm-score-analysis.hpp"
#include "sub-workflows/sm-memory-analysis.hpp"
#include "sub-workflows/sm-profile-analysis.hpp"
#include "sub-workflows/sm-perf-analysis.hpp"
#include "sub-workflows/sm-traces-analysis.hpp"


// number of runs
int number_of_runs = 3;

// output location
std::string output_base = "./analysis-results/";

int main(int argc, const char * argv[]) {

    // benchmark trace base path
    std::string bench_base = "./trace-example/lttng-traces/";

    // benchmark name(s)
    std::string bench = "scimark2";

    // directory result base name
    std::string results_base = "./trace-example/results/";


    Workflow w (bench);

    Constant<std::string> *bench_name = new Constant<std::string>(bench);
    Constant<std::string> *bench_path = new Constant<std::string>(bench_base);

    Constant<std::string> *score_path = new Constant<std::string>(results_base+"res-score/");
    Constant<std::string> *time_path = new Constant<std::string>(results_base+"res-time/");
    Constant<std::string> *trace_path = new Constant<std::string>(results_base);

    w.addModule(*bench_name);
    w.addModule(*bench_path);
    w.addModule(*score_path);
    w.addModule(*time_path);
    w.addModule(*trace_path);

    GetMemoryAllRuns *mall = new GetMemoryAllRuns;
    w.addModule(*mall);
    w.link(bench_name->constant_value_out, mall->bench_name);
    w.link(bench_path->constant_value_out, mall->bench_path);

    GetTimeAllAnalysis *tall = new GetTimeAllAnalysis;
    w.addModule(*tall);
    w.link(bench_name->constant_value_out, tall->bench_name);
    w.link(time_path->constant_value_out, tall->bench_path);

    GetScoreAllAnalysis *sall = new GetScoreAllAnalysis;
    w.addModule(*sall);
    w.link(bench_name->constant_value_out, sall->bench_name);
    w.link(score_path->constant_value_out, sall->bench_path);

    GetProfileAllRuns *pall = new GetProfileAllRuns;
    w.addModule(*pall);
    w.link(bench_name->constant_value_out, pall->bench_name);
    w.link(bench_path->constant_value_out, pall->bench_path);

    GetPerfAllRuns *perfall = new GetPerfAllRuns;
    w.addModule(*perfall);
    w.link(bench_name->constant_value_out, perfall->bench_name);
    w.link(bench_path->constant_value_out, perfall->bench_path);

    GetTraceAllAnalysis *traceall = new GetTraceAllAnalysis;
    w.addModule(*traceall);
    w.link(bench_name->constant_value_out, traceall->bench_name);
    w.link(trace_path->constant_value_out, traceall->bench_path);

    w.start();


    return 0;

}

