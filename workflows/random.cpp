
#include <swat/core/workflow.hpp>

#include <swat/utils/stdout.hpp>
#include <swat/utils/constant.hpp>
#include <swat/utils/random.hpp>

int main(int argc, const char * argv[]) {


    Workflow w;

    Constant<int> *num = new Constant<int>(42);
    w.addModule(*num);
   
    Random<int> *rand = new Random<int>;
    w.addModule(*rand);

    Stdout<int,Stream> *out = new Stdout<int,Stream>;
    w.addModule(*out);


    w.link(num->constant_value_out, rand->num_values);
    w.link(rand->random, out->data_stream);
        
    w.start();

    return 0;

}

