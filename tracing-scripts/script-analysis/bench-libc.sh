#!/bin/bash

if test $# -eq 0
then
   echo "Usage: $0 benchname"
   exit 1
fi 

bench=$1

# export variables for phoronix 
export FORCE_TIMES_TO_RUN=1
export TEST_RESULTS_NAME=$bench-libc

# dynamic library to trace memory funictions
export LD_PRELOAD="liblttng-ust-libc-wrapper.so"

## Bench + lttng trace
# create lttng session
lttng create $TEST_RESULTS_NAME-$TEST_RESULTS_IDENTIFIER

# create user channel
lttng enable-channel "user" -u --subbuf-size 256M
# add all events from libc
lttng enable-event -u "lttng_ust_libc:*" -c "user"
# add context information
#lttng add-context -u -t vtid -t procname -c "user"

# create kernel channel
lttng enable-channel "kernel" -k --subbuf-size 256M
# add kernel tracepoint to know the process tree
lttng enable-event -k sched_switch,sched_process_fork,sched_process_exec,sched_process_exit -c "kernel"
# add context info for kernel trace
#lttng add-context -k -t tid -c "kernel"

# add syscalls
lttng enable-event -k --syscall --all -c "kernel"

# start tracing
lttng start
# run phoronix + time
/usr/bin/time --portability -o ./res-time/$bench-libc-$TEST_RESULTS_IDENTIFIER phoronix-test-suite default-run $1
# stop tracing and destroy session
lttng stop
lttng destroy
