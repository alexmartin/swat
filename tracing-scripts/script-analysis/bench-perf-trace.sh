#!/bin/bash

if test $# -eq 0
then
   echo "Usage: $0 benchname"
   exit 1
fi 

bench=$1

# export variables for phoronix 
export FORCE_TIMES_TO_RUN=1
export TEST_RESULTS_NAME=$bench-perf-trace

## Bench + lttng trace
# create lttng session
lttng create $TEST_RESULTS_NAME-$TEST_RESULTS_IDENTIFIER

# create kernel channel
lttng enable-channel "kernel" -k --subbuf-size 256M
# add kernel tracepoint to know the process tree
lttng enable-event -k sched_switch,sched_process_fork,sched_process_exec,sched_process_exit -c "kernel"

# add syscalls
lttng enable-event -k --syscall --all -c "kernel"

# add context info for perf counters
lttng add-context -k -t perf:cpu:instructions -c "kernel"
lttng add-context -k -t perf:cpu:L1-dcache-loads -c "kernel"
lttng add-context -k -t perf:cpu:L1-dcache-stores -c "kernel"

# start tracing
lttng start
# run phoronix
/usr/bin/time --portability -o ./res-time/$bench-perf-trace-$TEST_RESULTS_IDENTIFIER phoronix-test-suite default-run $1
# stop tracing and destroy session
lttng stop
lttng destroy
