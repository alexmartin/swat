#!/bin/bash

if test $# -eq 0
then
   echo "Usage: $0 benchname"
   exit 1
fi 

bench=$1

# export variables for phoronix 
export FORCE_TIMES_TO_RUN=1
export TEST_RESULTS_NAME="$bench-perf-bench"

perf stat -e instructions,L1-dcache-loads,L1-dcache-stores -x , -o ./res-perf/$bench-perf-bench-$TEST_RESULTS_IDENTIFIER phoronix-test-suite default-run $1
