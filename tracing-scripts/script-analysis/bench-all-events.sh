#!/bin/bash

if test $# -eq 0
then
   echo "Usage: $0 benchname"
   exit 1
fi 

# set date and bench name variables
bench=$1

# export variables for phoronix 
export FORCE_TIMES_TO_RUN=1
export TEST_RESULTS_NAME=$bench-all-events

## Bench + lttng trace
# create lttng session
lttng create $TEST_RESULTS_NAME-$TEST_RESULTS_IDENTIFIER

# create kernel channel
lttng enable-channel "kernel" -k --subbuf-size 256M
# add all events in the kernel
lttng enable-event -a -k -c "kernel"

# start tracing
lttng start
# run phoronix + time
/usr/bin/time --portability -o ./res-time/$bench-all-events-$TEST_RESULTS_IDENTIFIER phoronix-test-suite default-run $1
# stop tracing and destroy session
lttng stop
lttng destroy
