#!/bin/bash

if test $# -eq 0
then
   echo "Usage: $0 benchname"
   exit 1
fi 


bench=$1

# export variables for phoronix 
export FORCE_TIMES_TO_RUN=1
export TEST_RESULTS_NAME="$bench-time"

/usr/bin/time --portability -o ./res-time/$bench-time-$TEST_RESULTS_IDENTIFIER phoronix-test-suite default-run $1
