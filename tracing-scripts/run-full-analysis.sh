#!/bin/bash

if test $# -lt 1
then
    echo "Usage: $0 test-scripts benchname" 
    exit 1
fi 

script=$1
benchname=$2

for test in $(ls -1 $1) 
do
    echo "test: $test"
    ./run-analysis.sh $1/$test $2
done
