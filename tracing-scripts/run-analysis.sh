#!/bin/bash

if test $# -lt 2
then
    echo "Usage: $0 test-script benchname" 
    exit 1
fi 

script=$1
benchname=$2

for r in {0..31}
do
    echo "run$r"
    export TEST_RESULTS_IDENTIFIER="run$r"
    $1 $2
    sync
done
