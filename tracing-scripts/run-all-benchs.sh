#!/bin/bash

if test $# -lt 2
then
    echo "Usage: $0 analysis-script analysis/folder" 
    exit 1
fi 

script=$1
analysis=$2

for bench in compress-gzip ffmpeg iozone network-loopback phpbench pybench ramspeed scimark2 stream unpack-linux
do
    echo "bench: $bench"
    $script $analysis $bench
done
