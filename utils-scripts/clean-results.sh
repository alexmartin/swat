#!/bin/bash

rm -rf ~/lttng-traces/*
rm -rf ~/.phoronix-test-suite/test-results/*
rm -rf res-time/*
rm -rf res-perf/*
rm -rf res-score/*
