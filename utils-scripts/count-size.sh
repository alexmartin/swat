#!/bin/bash

file=$1

for bench in compress-gzip ffmpeg idle iozone network-loopback phpbench pybench ramspeed scimark2 stream unpack-linux
do
    echo $bench
    for trace in all-events libc perf-trace
    do
        echo $trace
        for n in {0..31}
        do
            echo -n .
            du -s ./$file/$bench-$trace-run$n/ | awk {'print $1'} >> ./$bench-$trace-size.txt
        done
        echo
    done
done


