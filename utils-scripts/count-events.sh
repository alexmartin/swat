#!/bin/bash

file=$1

for bench in compress-gzip ffmpeg idle iozone network-loopback phpbench pybench ramspeed scimark2 stream unpack-linux
do

    echo $bench
    for trace in all-events perf-trace libc
    do

        echo -n $trace
        for n in {0..31}
        do

            echo -n -$n
            babeltrace ./$file/$bench-$trace-run$n/kernel/ | wc -l >> ./$bench-$trace.txt
            if [ trace == "libc" ]
            then
                babeltrace ./$file/$bench-$trace-run$n/ust/ | wc -l >> ./$bench-$trace.txt
            fi

        done
        echo
    
done


