#!/bin/bash

cd $1

for name in *;
do
    mv $name ${name:0:((${#name}-16))}
done
