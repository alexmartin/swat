#!/bin/bash

for bench in compress-gzip ffmpeg iozone network-loopback phpbench pybench ramspeed scimark2 stream unpack-linux
do
    for analysis in all-events libc perf-trace only time perf-wide perf-bench
    do
        name="$bench-$analysis"
        phoronix-test-suite result-file-to-csv "$name" | tail -n 3 | head -n 1 > ./res-score/"$name".csv
    done
done

